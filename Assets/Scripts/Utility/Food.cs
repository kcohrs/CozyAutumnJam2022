﻿using System;

namespace AutumnJam.Utility
{
    public class Food
    {
        #region Fields

        public FoodType Type;
        // float to allow for halving
        public float Amount;

        #endregion

        #region Constructors

        public Food(FoodType type, float amount)
        {
            Type = type;
            Amount = amount;
        }

        #endregion
    }

    [Serializable]
    public class FoodEfficiency
    {
        #region Fields

        public FoodType Type;
        public float Efficiency;

        #endregion

        #region Constructors

        public FoodEfficiency(FoodType type, float efficiency)
        {
            Type = type;
            Efficiency = efficiency;
        }

        #endregion
    }

    public enum FoodType
    {
        None,
        Carrot,
        AutumnBerry,
        Beans,
        Rhubarb,
        Hazelnuts,
        Grapes,
        Corn,
        Apples,
        Fish
    }
}
