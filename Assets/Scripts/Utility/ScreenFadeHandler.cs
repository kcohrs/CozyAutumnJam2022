using System.Collections;

using UnityEngine;
using UnityEngine.UI;

public class ScreenFadeHandler : MonoBehaviour
{
    #region Fields

    public static ScreenFadeHandler Instance;

    [SerializeField]
    private Image _blackScreen;

    #endregion

    #region Constructors

    public ScreenFadeHandler()
    {
        Instance = this;
    }

    #endregion

    #region Public Methods

    public IEnumerator FlashFadeScreen(float inSpeed, float blackTime, float outSpeed)
    {
        var alpha = 0f;
        _blackScreen.enabled = true;
        _blackScreen.gameObject.SetActive(true);
        _blackScreen.color = new Color(1, 1, 1, 0);

        while (_blackScreen.color.a < 0.9)
        {
            alpha += inSpeed;
            _blackScreen.color = new Color(1, 1, 1, alpha);
            yield return 0;
        }

        _blackScreen.color = Color.black;

        yield return new WaitForSeconds(blackTime);

        while (_blackScreen.color.a > 0.1)
        {
            alpha -= outSpeed;
            _blackScreen.color = new Color(1, 1, 1, alpha);
            yield return 0;
        }

        _blackScreen.color = new Color(1, 1, 1, 0f);
    }

    #endregion
}
