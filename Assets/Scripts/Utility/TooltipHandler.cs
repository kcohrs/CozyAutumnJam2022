using System.Linq;

using AutumnJam.Field.Placeables;

using TMPro;

using UnityEngine;
using UnityEngine.UI;

namespace AutumnJam.Utility
{
    public class TooltipHandler : MonoBehaviour
    {
        #region Constants

        private const float Padding = 8f;

        #endregion

        #region Fields

        [SerializeField]
        private RectTransform _canvas;

        [SerializeField]
        private RectTransform _textTooltip;
        [SerializeField]
        private TextMeshProUGUI _tooltipText;
        [SerializeField]
        private RectTransform _tooltipRect;

        [SerializeField]
        private RectTransform _placeableTooltip;
        [SerializeField]
        private RectTransform _placeableRect;
        [SerializeField]
        private TextMeshProUGUI _placeableName;
        [SerializeField]
        private Image _placeableIcon;
        [SerializeField]
        private TextMeshProUGUI _placeableDescription;

        [SerializeField]
        private RectTransform _productionTooltip;
        [SerializeField]
        private RectTransform _productionRect;
        [SerializeField]
        private TextMeshProUGUI _productionName;
        [SerializeField]
        private Image _productionIcon;
        [SerializeField]
        private TextMeshProUGUI _productionDescription;
        [SerializeField]
        private TextMeshProUGUI _productionPrice;
        [SerializeField]
        private TextMeshProUGUI _productionTimeToGrow;
        [SerializeField]
        private TextMeshProUGUI _productionYield;
        [SerializeField]
        private TextMeshProUGUI _productionSurvivesWinter;

        [SerializeField]
        private RectTransform _housingTooltip;
        [SerializeField]
        private RectTransform _housingRect;
        [SerializeField]
        private TextMeshProUGUI _housingName;
        [SerializeField]
        private Image _housingIcon;
        [SerializeField]
        private TextMeshProUGUI _housingDescription;
        [SerializeField]
        private TextMeshProUGUI _housingPrice;
        [SerializeField]
        private TextMeshProUGUI _housingFood;
        [SerializeField]
        private TextMeshProUGUI _housingVictory;
        [SerializeField]
        private TextMeshProUGUI _housingSurvivesWinter;

        [SerializeField]
        private RectTransform _effectTooltip;
        [SerializeField]
        private RectTransform _effectRect;
        [SerializeField]
        private TextMeshProUGUI _effectName;
        [SerializeField]
        private Image _effectIcon;
        [SerializeField]
        private TextMeshProUGUI _effectDescription;
        [SerializeField]
        private TextMeshProUGUI _effectPrice;
        [SerializeField]
        private TextMeshProUGUI _effectSurvivesWinter;

        private bool right;
        private bool top;

        #endregion

        #region Constructors

        public TooltipHandler()
        {
            Instance = this;
        }

        #endregion

        #region Properties

        public static TooltipHandler Instance { get; private set; }

        #endregion

        #region Public Methods

        public void ShowTooltip(string text)
        {
            _textTooltip.gameObject.SetActive(true);
            _tooltipText.text = text;
        }

        public void ShowTooltip(Placeable placeable, bool isGamePlay)
        {
            // todo account of gameplay objects

            if (placeable is ProductionPlaceable production)
            {
                _productionTooltip.gameObject.SetActive(true);
                _productionName.text = placeable.Name;
                _productionIcon.sprite = placeable.ToolTipSprite;
                _productionDescription.text = placeable.Description;

                _productionPrice.text = $"{placeable.Price}";

                if (placeable.GrowthStages.Any())
                {
                    var timeToGrow = placeable.GrowthStages.FindLast(x => !x.ActiveEffect);
                    _productionTimeToGrow.text = $"{timeToGrow.EndTurn}";
                }

                _productionYield.text = $"{production.FoodPerTurn}";
                _productionSurvivesWinter.text = production.SurvivesWinter ? "Yes" : "No";
                return;
            }

            if (placeable is HousingPlaceable housing)
            {
                _housingTooltip.gameObject.SetActive(true);
                _housingName.text = placeable.Name;
                _housingIcon.sprite = placeable.ToolTipSprite;
                _housingDescription.text = placeable.Description;

                _housingPrice.text = $"{placeable.Price}";
                _housingFood.text = $"{housing.RequiredFood}";
                _housingVictory.text = $"{housing.VictoryPoints}";
                _housingSurvivesWinter.text = housing.SurvivesWinter ? "Yes" : "No";
                return;
            }

            if (placeable is EffectPlaceable effect)
            {
                _effectTooltip.gameObject.SetActive(true);
                _effectName.text = placeable.Name;
                _effectIcon.sprite = placeable.ToolTipSprite;
                _effectDescription.text = placeable.Description;

                _effectPrice.text = $"{placeable.Price}";
                _effectSurvivesWinter.text = effect.SurvivesWinter ? "Yes" : "No";
                return;
            }

            // default case
            _placeableTooltip.gameObject.SetActive(true);
            _placeableName.text = placeable.Name;
            _placeableIcon.sprite = placeable.ToolTipSprite;
            _placeableDescription.text = placeable.Description;
        }

        public void HideTooltip()
        {
            if (_textTooltip != null)
                _textTooltip.gameObject?.SetActive(false);
            if (_placeableTooltip != null)
                _placeableTooltip?.gameObject?.SetActive(false);
            if (_productionTooltip != null)
                _productionTooltip?.gameObject?.SetActive(false);
            if (_housingTooltip != null)
                _housingTooltip?.gameObject?.SetActive(false);
            if (_effectTooltip != null)
                _effectTooltip?.gameObject?.SetActive(false);
        }

        #endregion

        #region Non-Public Methods

        private void Awake()
        {
            _textTooltip.gameObject.SetActive(false);
            _productionTooltip.gameObject.SetActive(false);
            _placeableTooltip.gameObject.SetActive(false);
            _housingTooltip.gameObject.SetActive(false);
            _effectTooltip.gameObject.SetActive(false);

            var target = Input.mousePosition;
            right = target.x / Screen.width > 0.5f;
            top = target.y / Screen.height > 0.5f;
        }

        private void Update()
        {
            var target = Input.mousePosition;
            Vector2 localPoint;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_canvas, target, null, out localPoint);

            if (!right && target.x / Screen.width > 0.5f)
            {
                // right
                var vec = new Vector2(0, _tooltipRect.anchorMin.y);
                _tooltipRect.anchorMin = vec;
                _tooltipRect.anchorMax = vec;
                _placeableRect.anchorMin = vec;
                _placeableRect.anchorMax = vec;
                _productionRect.anchorMin = vec;
                _productionRect.anchorMax = vec;
                _housingRect.anchorMin = vec;
                _housingRect.anchorMax = vec;
                _effectRect.anchorMin = vec;
                _effectRect.anchorMax = vec;
                right = true;
            }

            if (right && target.x / Screen.width < 0.5f)
            {
                // left
                var vec = new Vector2(1, _tooltipRect.anchorMin.y);
                _tooltipRect.anchorMin = vec;
                _tooltipRect.anchorMax = vec;
                _placeableRect.anchorMin = vec;
                _placeableRect.anchorMax = vec;
                _productionRect.anchorMin = vec;
                _productionRect.anchorMax = vec;
                _housingRect.anchorMin = vec;
                _housingRect.anchorMax = vec;
                _effectRect.anchorMin = vec;
                _effectRect.anchorMax = vec;
                right = false;
            }

            if (!top && target.y / Screen.height > 0.5f)
            {
                // top
                var vec = new Vector2(_tooltipRect.anchorMin.x, 0);
                _tooltipRect.anchorMin = vec;
                _tooltipRect.anchorMax = vec;
                _placeableRect.anchorMin = vec;
                _placeableRect.anchorMax = vec;
                _productionRect.anchorMin = vec;
                _productionRect.anchorMax = vec;
                _housingRect.anchorMin = vec;
                _housingRect.anchorMax = vec;
                _effectRect.anchorMin = vec;
                _effectRect.anchorMax = vec;
                top = true;
            }

            if (top && target.y / Screen.height < 0.5f)
            {
                // bottom
                var vec = new Vector2(_tooltipRect.anchorMin.x, 1);
                _tooltipRect.anchorMin = vec;
                _tooltipRect.anchorMax = vec;
                _placeableRect.anchorMin = vec;
                _placeableRect.anchorMax = vec;
                _productionRect.anchorMin = vec;
                _productionRect.anchorMax = vec;
                _housingRect.anchorMin = vec;
                _housingRect.anchorMax = vec;
                _effectRect.anchorMin = vec;
                _effectRect.anchorMax = vec;
                top = false;
            }

            var placeableOffset = new Vector2(right ? -50 : 50, top ? -50 : 50);
            _textTooltip.localPosition = localPoint;
            _productionTooltip.localPosition = placeableOffset + localPoint;
            _placeableTooltip.localPosition = placeableOffset + localPoint;
            _housingTooltip.localPosition = placeableOffset + localPoint;
            _effectTooltip.localPosition = placeableOffset + localPoint;
        }

        #endregion
    }
}
