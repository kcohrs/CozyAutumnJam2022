using System.Collections;

using AutumnJam.Field.Placeables;

using UnityEngine;
using UnityEngine.EventSystems;

namespace AutumnJam.Utility
{
    public class MouseOverHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        #region Fields

        [SerializeField]
        private string _tooltip;

        [SerializeField]
        private Placeable _tooltipPlaceable;

        private bool _show;

        #endregion

        #region Public Methods

        public void OnPointerEnter(PointerEventData eventData)
        {
            _show = true;
            if (string.IsNullOrEmpty(_tooltip) && _tooltipPlaceable == null)
                return;

            StartCoroutine(ShowAfterDelay(false));
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _show = false;
            TooltipHandler.Instance.HideTooltip();
        }

        public void OnDisable()
        {
            _show = false;
            TooltipHandler.Instance.HideTooltip();
        }

        public void SetupPlaceable(Placeable occupiedBy)
        {
            _tooltipPlaceable = occupiedBy;
        }

        public void ResetPlaceable()
        {
            _tooltipPlaceable = null;
        }

        #endregion

        #region Non-Public Methods

        private void OnMouseOver()
        {
            if (!GameStateManager.Instance.IsPlaying || ProgressManager.Instance.IsEndingYear || HudManager.Instance.IsMouseOverUI)
                return;

            _show = true;

            if (string.IsNullOrEmpty(_tooltip) && _tooltipPlaceable == null)
                return;

            StartCoroutine(ShowAfterDelay(true));
        }

        private void OnMouseExit()
        {
            if (!GameStateManager.Instance.IsPlaying)
                return;

            _show = false;
            TooltipHandler.Instance.HideTooltip();
        }

        private IEnumerator ShowAfterDelay(bool isGamePlay)
        {
            yield return new WaitForSeconds(0.1f);

            if (_show)
            {
                if (_tooltipPlaceable != null)
                    TooltipHandler.Instance.ShowTooltip(_tooltipPlaceable, isGamePlay);
                else
                    TooltipHandler.Instance.ShowTooltip(_tooltip);
            }
        }

        #endregion
    }
}
