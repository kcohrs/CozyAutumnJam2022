using System.Collections.Generic;
using System.Linq;

using UnityEngine;

namespace AutumnJam.Utility
{
    public class BackgroundLightMover : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        private Vector3 _direction;

        [SerializeField]
        private GameObject _lightPrefab;

        private List<GameObject> _currentLights;

        #endregion

        #region Non-Public Methods

        void Start()
        {
            _currentLights = new List<GameObject>();
            var baseLight = Instantiate(_lightPrefab, Vector3.zero, Quaternion.identity, transform);
            _currentLights.Add(baseLight);
        }

        // Update is called once per frame
        void LateUpdate()
        {
            // instead, move this, with child lights
            transform.Translate(_direction);

            var toDelete = new List<GameObject>();
            var leftMost = _currentLights.First();

            foreach (var obj in _currentLights)
            {
                obj.transform.Translate(_direction);

                if (obj.transform.position.x > 250f)
                {
                    toDelete.Add(obj);
                    continue;
                }

                if (obj.transform.position.x < leftMost.transform.position.x)
                    leftMost = obj;
            }

            while (toDelete.Any())
            {
                var obj = toDelete.First();
                _currentLights.Remove(obj);
                Destroy(obj.gameObject);
                toDelete.Remove(obj);
            }

            if (leftMost.transform.position.x > -50f)
            {
                // should be enough of a buffer for widescreens
                var baseLight = Instantiate(_lightPrefab, leftMost.transform.position - new Vector3(50f, 0, 0), Quaternion.identity, transform);
                _currentLights.Add(baseLight);
            }

            // once past a certain point, substract -500 (10 steps) to move back
            if (transform.position.x > 250f)
                transform.Translate(transform.position - new Vector3(-500f, 0f));
        }

        #endregion
    }
}
