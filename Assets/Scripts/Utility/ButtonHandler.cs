using AutumnJam;

using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonHandler : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler
{
    #region Public Methods

    public void OnPointerEnter(PointerEventData eventData)
    {
        AudioManager.Instance.PlaySound(AudioManager.Sound.Hover);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        AudioManager.Instance.PlaySound(AudioManager.Sound.ClickUi);
    }

    #endregion
}
