﻿using System.Collections;

using TMPro;

using UnityEngine;
using UnityEngine.UI;

namespace AutumnJam.Utility
{
    public class IntroHandler : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        private GameObject _mainMenu;
        [SerializeField]
        private Image _blackScreen;
        [SerializeField]
        private TextMeshProUGUI _introText1;
        [SerializeField]
        private TextMeshProUGUI _introText2;
        [SerializeField]
        private TextMeshProUGUI _introText3;
        [SerializeField]
        private TextMeshProUGUI _mainText;
        private bool _finishedTitle;

        [SerializeField]
        private bool _skipIntro;

        #endregion

        #region Public Methods

        public void PlayIntro()
        {
            StartCoroutine(HandleIntro());
        }

        #endregion

        #region Non-Public Methods

        private IEnumerator HandleIntro()
        {
            _mainMenu.SetActive(false);
            _blackScreen.enabled = true;
            _introText1.enabled = false;
            _introText2.enabled = false;
            _introText3.enabled = false;
            _mainText.enabled = false;

            if (!_skipIntro)
            {
                // title card after 5 seconds

                yield return new WaitForSeconds(1f);

                _introText1.enabled = true;
                yield return new WaitForSeconds(2f);

                _introText1.enabled = false;
                _introText2.enabled = true;
                yield return new WaitForSeconds(2f);

                _introText2.enabled = false;
                _introText3.enabled = true;
                yield return new WaitForSeconds(2.5f);
            }
            else
                _introText3.enabled = true;

            StartCoroutine(MoveTitleUp());

            // we could tween here, but that will be more annoying
            var alpha = 1f;

            while (_blackScreen.color.a > 0.1)
            {
                if (_finishedTitle && !GameStateManager.Instance.IsPlaying)
                {
                    _mainMenu.SetActive(true);
                    break;
                }

                alpha -= 0.01f;
                _blackScreen.color = new Color(1, 1, 1, alpha);
                yield return 0;
            }

            _blackScreen.color = new Color(1, 1, 1, 0f);

            if (!GameStateManager.Instance.IsPlaying)
                _mainMenu.SetActive(true);

            yield return 0;
        }

        private IEnumerator MoveTitleUp()
        {
            while (_introText3.rectTransform.anchoredPosition3D.y < _mainText.rectTransform.anchoredPosition3D.y)
            {
                _introText3.transform.position = new Vector3(_introText3.transform.position.x, _introText3.transform.position.y + 3f,
                                                             _introText3.transform.position.z);
                yield return 0;
            }

            _finishedTitle = true;
            _mainText.enabled = true;
            _introText3.enabled = false;
        }

        #endregion
    }
}
