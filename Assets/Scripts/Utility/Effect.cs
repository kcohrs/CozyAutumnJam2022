﻿namespace AutumnJam.Utility
{
    public enum EffectType
    {
        None,
        DecreasedGrowthTime,
        IncreasedFoodYield,
        AutoHappiness,
        IncreasedEffects,
        WinterImmortal,
        IncreasedPickup,
        IncreasedSeasonTime,
    }
}
