using UnityEngine;

using static iTween;

namespace AutumnJam.Utility
{
    public class CameraMovement : MonoBehaviour
    {
        #region Fields

        private Vector3 _origin;

        #endregion

        #region Non-Public Methods

        void Start()
        {
            _origin = transform.position;
            InvokeRepeating("GetRandomCameraOffset", 0f, 2f);
        }

        private void GetRandomCameraOffset()
        {
            var newPosition = _origin + Random.insideUnitSphere * 0.1f;
            MoveTo(gameObject, Hash("position", newPosition, "time", 10f, "easetype", EaseType.easeInOutSine));
        }

        #endregion
    }
}
