using AutumnJam;

using UnityEngine;
using UnityEngine.EventSystems;

public class ScreenClickBlocker : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    #region Public Methods

    public void OnPointerEnter(PointerEventData eventData)
    {
        HudManager.Instance.IsMouseOverUI = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        HudManager.Instance.IsMouseOverUI = false;
    }

    #endregion
}
