using UnityEngine;

namespace AutumnJam.Utility
{
    public class AudioEffect : MonoBehaviour
    {
        #region Public Methods

        public void Setup(AudioClip clip)
        {
            var audioSource = GetComponent<AudioSource>();
            audioSource.clip = clip;
            audioSource.Play();
        }

        #endregion
    }
}
