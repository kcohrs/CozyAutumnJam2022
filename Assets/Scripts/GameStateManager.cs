using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using AutumnJam.Scoring;
using AutumnJam.Utility;
using LootLocker.Requests;

using UnityEditor;

using UnityEngine;

namespace AutumnJam
{
    /// <summary>
    /// Horrible cluster class
    /// </summary>
    public class GameStateManager : MonoBehaviour
    {
        #region Constants

        private const string SaveFileName = "save.dat";

        #endregion

        #region Fields

        [SerializeField]
        public bool IsPlaying;

        private ProgressManager _progressManager;
        private HudManager _hudManager;

        [SerializeField]
        private IntroHandler _introHandler;
        [SerializeField]
        private AudioManager _audioManager;

        [SerializeField]
        private Tutorial _tutorial;
        [SerializeField]
        private float _startingFood;

        public Leaderboard leaderboard;

        #endregion

        #region Constructors

        public GameStateManager()
        {
            Instance = this;
        }

        #endregion

        #region Properties

        // lmao singleton funni
        public static GameStateManager Instance { get; private set; }

        #endregion

        #region Public Methods

        public void StartNewGame()
        {
            StartCoroutine(ScreenFadeHandler.Instance.FlashFadeScreen(0.1f, 0.1f, 0.05f));

            StartCoroutine(SetupGame());
        }

        public void ExitGame()
        {
            Application.Quit();

#if UNITY_EDITOR
            EditorApplication.ExitPlaymode();
#endif
        }

        public void AbortCurrentGame()
        {
            IsPlaying = false;
            TileManager.Instance.ToggleWinter(false);
            _audioManager.PlayBackgroundMusic(AudioManager.BackgroundMusic.Menu);
            _hudManager.QuitGameToMenu();
        }

        public void EndCurrentGame(int score)
        {
            IsPlaying = false;
        }

        #endregion

        #region Non-Public Methods

        //waits for player login and then fetches highscorelist
        private IEnumerator SetupRoutine()
        {
            yield return LoginRoutine();
            yield return _hudManager.FetchTopHighscoresRoutine();
        }

        //logs the player into LootLocker
        private IEnumerator LoginRoutine()
        {
            bool done = false;
            LootLockerSDKManager.StartGuestSession((response) =>
            {
                Debug.Log(response.success);
                if (response.success)
                {
                    Debug.Log("Player was logged in");
                    PlayerPrefs.SetString("PlayerID", response.player_id.ToString());
                    done = true;
                }
                else
                {
                    Debug.Log("Could not start session");
                    done = true;
                }
            });
            yield return new WaitWhile(() => done == false);
        }

            private IEnumerator SetupGame()
        {
            yield return new WaitForSeconds(.15f);

            _progressManager.ClearFieldState(true);

            _progressManager.ResetProgress();
            _audioManager.PlayBackgroundMusic(AudioManager.BackgroundMusic.Gameplay1);

            IsPlaying = true;
            StartCoroutine(TileManager.Instance.LockAllTiles());
            yield return 0;

            _progressManager.StartNextYear();
            _progressManager.AddPickup(new Food(FoodType.Apples, _startingFood), 0);

            yield return 0;

            _hudManager.StartNewGame();
            _hudManager.UpdateUnlocks();
            yield return 0;

            _tutorial.StartTutorial();
        }

        private void Start()
        {
            Application.targetFrameRate = 60;

            DontDestroyOnLoad(gameObject);
            _progressManager = GetComponent<ProgressManager>();
            _progressManager.SetupRandomBackground();

            _hudManager = GetComponent<HudManager>();
            StartCoroutine(SetupRoutine());

            _audioManager.PlayBackgroundMusic(AudioManager.BackgroundMusic.Menu, true);
            _introHandler.PlayIntro();

            TileManager.Instance.UnlockAllTiles();
        }

        #endregion
    }
}
