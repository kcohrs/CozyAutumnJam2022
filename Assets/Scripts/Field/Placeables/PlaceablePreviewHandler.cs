﻿using UnityEngine;

namespace AutumnJam.Field.Placeables
{
    public class PlaceablePreviewHandler : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        public Placeable Info;
        private SpriteRenderer _spriteRenderer;

        #endregion

        #region Public Methods

        public void PreviewPlaceable(Placeable placeable)
        {
            Info = placeable;
            _spriteRenderer.sprite = placeable.ToolTipSprite;

            if (placeable.Category == PlaceableCategory.Shovel)
                _spriteRenderer.color = Color.white;
            else
                _spriteRenderer.color = new Color(1f, 1f, 1f, 0.3f);
        }

        public void MarkForbidden()
        {
            _spriteRenderer.color = new Color(0f, 0f, 0f, 0.3f);
        }

        #endregion

        #region Non-Public Methods

        private void Awake()
        {
            _spriteRenderer ??= GetComponent<SpriteRenderer>();
        }

        #endregion
    }
}
