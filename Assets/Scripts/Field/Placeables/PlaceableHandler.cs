using System;
using System.Collections.Generic;
using System.Linq;

using AutumnJam.Utility;

using UnityEngine;
using UnityEngine.UI;

namespace AutumnJam.Field.Placeables
{
    public class PlaceableHandler : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        public Placeable Info;

        private Animator _animation;
        private SpriteRenderer _spriteRenderer;
        [SerializeField]
        private Slider _happinessSlider;
        [SerializeField]
        private Image _wasJustPlaced;

        // float because growth speedup adds uneven values
        private float _age;
        private GameObject _detailObject;

        #endregion

        #region Public Methods

        public void FadeOut()
        {
            var color = _spriteRenderer.color;
            color.a = 0.5f;
            _spriteRenderer.color = color;
        }

        public void ResetFadeOut()
        {
            var color = _spriteRenderer.color;
            color.a = 1f;
            _spriteRenderer.color = color;
        }

        public void SetupPlaceable(Placeable placeable)
        {
            Info = placeable;
            if (GameStateManager.Instance.IsPlaying)
                _wasJustPlaced.gameObject.SetActive(true);

            if (Info.GrowthStages.Any())
            {
                Info.CurrentGrowthStage = Info.GrowthStages.FirstOrDefault();
                SetAnimation(Info.CurrentGrowthStage?.AnimationName);
            }
            else
            {
                Debug.LogError($"No growth stages set for {Info.Name}.");
                Info.SlatedForDeletion = true;
            }
        }

        public SurroundingState GetSurroundingState(FieldState state, int x, int y, int rangeLimit = 3)
        {
            var surroundings = new SurroundingState();

            foreach (var plot in state.State)
            {
                // get all global effects and tick them if needed
                if (plot.IsGlobalEffect)
                {
                    if (!plot.TickedThisYear)
                        plot.Tick();

                    var plotSurroundings = plot.GetAdjacentEffectorsOfType(EffectType.IncreasedEffects);
                    var placeable = plot.OccupiedBy as EffectPlaceable;
                    surroundings.AvailableEffects.Add((placeable.Effect, 0, plotSurroundings.Any()));

                    // and then get effect somehow
                }

                // get all surrounding effects and tick them if needed
                if (Math.Abs(plot.PosX - x) <= rangeLimit && Math.Abs(plot.PosY - y) <= rangeLimit)
                {
                    if (plot.PosX == x && plot.PosY == y)
                        continue;

                    if (plot.IsLocalEffect)
                    {
                        if (!plot.TickedThisYear)
                            plot.Tick();

                        var placeable = plot.OccupiedBy as EffectPlaceable;

                        if (placeable.IsEffectActive)
                        {
                            var distance = Math.Max(Math.Abs(plot.PosX - x), Math.Abs(plot.PosY - y));

                            switch (distance)
                            {
                                case 0: // self,
                                    break;

                                case 1:
                                    var plotSurroundings = plot.GetAdjacentEffectorsOfType(EffectType.IncreasedEffects);
                                    surroundings.AvailableEffects.Add((placeable.Effect, distance, plotSurroundings.Any()));
                                    break;

                                case 2:
                                    plotSurroundings = plot.GetAdjacentEffectorsOfType(EffectType.IncreasedEffects);
                                    if (placeable.Effect == EffectType.WinterImmortal && plotSurroundings.Any())
                                        surroundings.AvailableEffects.Add((placeable.Effect, distance, true));

                                    break;

                                case 3: // ignore
                                    break;
                            }
                        }
                    }

                    // additionally tick all adjacent housing just to be sure its up to date
                    if (Info.Category == PlaceableCategory.Production && plot.IsHousing && !plot.TickedThisYear)
                        plot.Tick();
                }
            }

            return surroundings;
        }

        public void Tick(FieldState state, int x, int y)
        {
            if (Info.SlatedForDeletion)
            {
                if (GameStateManager.Instance.IsPlaying)
                    Debug.LogWarning("Placeable that is slated for deletion ticked.");
                return;
            }
            // COLLECT ALL EFFECTS

            #region EFFECTS

            var surroundings = GetSurroundingState(state, x, y);

            #endregion

            // HANDLE EFFECTS

            #region HANDLEEFFECTS

            // GROWTH
            var boosters = 0;
            var lastAge = _age;
            var growthIncreasers = surroundings.AvailableEffects.Count(x => x.Effect == EffectType.DecreasedGrowthTime);

            if (growthIncreasers > 0)
                boosters = surroundings.AvailableEffects.Count(x => x.Effect == EffectType.DecreasedGrowthTime && x.Boosted);
            _age += 1 + growthIncreasers * (1 + boosters * 0.5f) / 3f; // 

            #endregion

            #region GROWTH

            var skipTurnResult = PlotResult.None;

            if (Math.Floor(_age) - Math.Floor(lastAge) > 1)
            {
                // we skipped a year
                _age -= 1;

                if (Info.CurrentGrowthStage.EndTurn <= _age)
                {
                    if (Info.CurrentGrowthStage.RevertToTurn > -1)
                        _age = Info.CurrentGrowthStage.RevertToTurn;

                    if (_detailObject != null)
                    {
                        Destroy(_detailObject);
                        _detailObject = null;
                    }

                    Info.SetNextGrowthStage(_age);

                    if (Info.CurrentGrowthStage != null)
                    {
                        SetAnimation(Info.CurrentGrowthStage.AnimationName);

                        if (Info.CurrentGrowthStage.DetailObjectPrefab != null)
                            _detailObject = Instantiate(Info.CurrentGrowthStage.DetailObjectPrefab, transform);
                    }
                    else
                    {
                        // kill the plant 
                        Info.SlatedForDeletion = true;
                        return;
                    }
                }

                if (Info.Category == PlaceableCategory.Production)
                    Debug.Log($"{Info.Name} ({x}/{y}) skipped a year.");

                skipTurnResult = Info.EndTurn(state, x, y, surroundings, _age);
                _age += 1;
            }

            if (Info.CurrentGrowthStage.EndTurn <= _age)
            {
                if (Info.CurrentGrowthStage.RevertToTurn > -1)
                    _age = Info.CurrentGrowthStage.RevertToTurn;

                if (_detailObject != null)
                {
                    Destroy(_detailObject);
                    _detailObject = null;
                }

                Info.SetNextGrowthStage(_age);

                if (Info.CurrentGrowthStage != null)
                {
                    SetAnimation(Info.CurrentGrowthStage.AnimationName);

                    if (Info.CurrentGrowthStage.DetailObjectPrefab != null)
                        _detailObject = Instantiate(Info.CurrentGrowthStage.DetailObjectPrefab, transform);
                }
                else
                {
                    // kill the plant 
                    Info.SlatedForDeletion = true;
                    return;
                }
            }

            #endregion

            if (!GameStateManager.Instance.IsPlaying)
            {
                _wasJustPlaced.gameObject.SetActive(false);
                return;
            }

            // TICK

            var placeableResult = Info.EndTurn(state, x, y, surroundings, _age);

            if (skipTurnResult.Food.Type != FoodType.None)
                placeableResult.Food = new Food(skipTurnResult.Food.Type, placeableResult.Food.Amount + skipTurnResult.Food.Amount);
            placeableResult.VictoryPoints += skipTurnResult.VictoryPoints;

            #region HANDLE FOOD

            if (Info.Category == PlaceableCategory.Production && placeableResult.Food != null)
            {
                // if we produced food this turn
                var targetHousing = GetMostViableHousing(state, x, y, placeableResult);

                if (targetHousing.Any())
                {
                    // halve produced food and spawn other
                    // do neat animation from to our plot to target
                    placeableResult.Food.Amount /= 2;

                    PickUpManager.Instance.AnimateHousePickUp(placeableResult.Food, transform.position, targetHousing);
                }

                PickUpManager.Instance.SpawnPlayerFoodPickUp(transform.position + new Vector3(0f, 0.25f, -.1f), placeableResult.Food.Amount,
                                                             placeableResult.Food.Type);
            }

            #endregion

            if (placeableResult.VictoryPoints > 0)
                PickUpManager.Instance.SpawnPlayerVictoryPointPickUp(transform.position, placeableResult.VictoryPoints);

            if (Info.WasJustPlaced)
                _wasJustPlaced.gameObject.SetActive(true);
            else
                _wasJustPlaced.gameObject.SetActive(false);
        }

        public void ResetToPostWinter()
        {
            _age = Info.StartYearAfterWinter;

            if (Info is HousingPlaceable housing)
                housing.ResetFood();

            if (_detailObject != null)
            {
                Destroy(_detailObject);
                _detailObject = null;
            }

            Info.SetNextGrowthStage(_age);

            if (Info.CurrentGrowthStage != null)
            {
                SetAnimation(Info.CurrentGrowthStage.AnimationName);

                if (Info.CurrentGrowthStage.DetailObjectPrefab != null)
                    _detailObject = Instantiate(Info.CurrentGrowthStage.DetailObjectPrefab, transform);
            }
        }

        public void SetToWinter()
        {
            if (Info is HousingPlaceable housing)
                housing.ResetFood();

            if (Info.WinterAnimation != null)
                SetAnimation(Info.WinterAnimation);
        }

        #endregion

        #region Non-Public Methods

        private void Update()
        {
            if (Info.BarProgress > 0f || _happinessSlider.value > 0f)
            {
                if (_happinessSlider.value != Info.BarProgress)
                    _happinessSlider.value = Info.BarProgress;

                if (!_happinessSlider.gameObject.activeInHierarchy && Info.BarProgress > 0f)
                    _happinessSlider.gameObject.SetActive(true);

                if (_happinessSlider.gameObject.activeInHierarchy && Info.BarProgress <= 0f)
                    _happinessSlider.gameObject.SetActive(false);
            }
        }

        private List<FieldPlot> GetMostViableHousing(FieldState state, int x, int y, PlotResult placeableResult)
        {
            var foodType = placeableResult.Food.Type;
            var fields = new List<FieldPlot>();

            foreach (var plot in state.State)
            {
                var distance = Math.Max(Math.Abs(plot.PosX - x), Math.Abs(plot.PosY - y));

                if (distance <= 3)
                {
                    if (plot.IsHousing && ((HousingPlaceable)plot.OccupiedBy).RequiredFood - ((HousingPlaceable)plot.OccupiedBy).CurrentFood > 0f)
                    {
                        switch (distance)
                        {
                            case 1:
                                fields.Add(plot);
                                break;

                            case 2:
                                // only if adjacent to rangeboost
                                var surroundings = plot.GetSurroundingState(1);
                                if (surroundings.AvailableEffects.Any(k => k.Effect == EffectType.IncreasedPickup))
                                    fields.Add(plot);
                                break;

                            case 3:
                                // only if adjacent to rangeboost which is adjacent to effect boost
                                surroundings = plot.GetSurroundingState(2);

                                if (surroundings.AvailableEffects.Any(x => x.Effect == EffectType.IncreasedPickup))
                                {
                                    var effectorPlot = plot.GetAdjacentEffectorsOfType(EffectType.IncreasedPickup);

                                    if (effectorPlot.Any())
                                    {
                                        foreach (var pickup in effectorPlot)
                                        {
                                            var pickupSurroundings = pickup.GetSurroundingState(1);
                                            if (pickupSurroundings.AvailableEffects.Any(k => k.Effect == EffectType.IncreasedEffects))
                                                fields.Add(plot);
                                        }
                                    }
                                    else
                                        Debug.LogWarning("Surrounding mismatch");
                                }

                                break;
                        }
                    }
                }
            }

            if (!fields.Any())
                return fields;

            // order by efficiency for the food type
            var grouped = fields.GroupBy(x => ((HousingPlaceable)x.OccupiedBy).Efficiencies.Any(x => x.Type == foodType)
                                             ? ((HousingPlaceable)x.OccupiedBy).Efficiencies.First(y => y.Type == foodType).Efficiency
                                             : ((HousingPlaceable)x.OccupiedBy).DefaultFoodEfficiency)
                                .OrderByDescending(x => x.Key);

            return grouped.FirstOrDefault().ToList();
        }

        private void SetAnimation(string animation)
        {
            // there is a chance that we trigger the winter animation after an object died, so check here

            if (_animation != null)
                _animation.Play(animation);
        }

        void Awake()
        {
            _animation ??= GetComponent<Animator>();
            _spriteRenderer ??= GetComponent<SpriteRenderer>();
        }

        #endregion
    }
}
