using System.Linq;

using AutumnJam.Utility;

using UnityEngine;

namespace AutumnJam.Field.Placeables
{
    [CreateAssetMenu(fileName = "Placeable", menuName = "CozyAutumnObjects/Production", order = 2)]
    public class ProductionPlaceable : Placeable
    {
        #region Fields

        public FoodType FoodType;
        public int FoodPerTurn;
        public int VictoryPointsPerTurn;

        #endregion

        #region Public Methods

        public override PlotResult EndTurn(FieldState field, int x, int y, SurroundingState surroundingState, float age)
        {
            var result = base.EndTurn(field, x, y, surroundingState, age);

            if (CurrentGrowthStage.ActiveEffect)
            {
                var yieldIncreasers = surroundingState.AvailableEffects.Count(x => x.Effect == EffectType.IncreasedFoodYield);
                var boosters = surroundingState.AvailableEffects.Count(x => x.Effect == EffectType.IncreasedFoodYield && x.Boosted);

                if (yieldIncreasers > 0)
                    Debug.Log($"{Name} is boosted by {yieldIncreasers} composts and {boosters} boosters.");

                var additional = yieldIncreasers * 0.2f * FoodPerTurn + boosters * FoodPerTurn * 0.2f;
                result.Food = new Food(FoodType, FoodPerTurn + additional); // +50% per
                result.VictoryPoints = VictoryPointsPerTurn;
            }

            return result;
        }

        #endregion
    }
}
