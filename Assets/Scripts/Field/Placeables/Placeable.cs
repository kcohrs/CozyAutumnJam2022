using System;
using System.Collections.Generic;
using System.Linq;

using AutumnJam.Utility;

using UnityEngine;

namespace AutumnJam.Field.Placeables
{
    [CreateAssetMenu(fileName = "Placeable", menuName = "CozyAutumnObjects/Primitive", order = 1)]
    public class Placeable : ScriptableObject
    {
        #region Fields

        public string Name;

        public PlaceableCategory Category;
        public Sprite ToolTipSprite;
        public string WinterAnimation;
        [TextArea]
        public string Description;
        public bool LimitedToWater;
        public int Price;
        public int UnlockPrice;
        public int UnlockYear;
        /// <summary>
        /// There is no easy way to pass animation + anim controller via scriptables.
        /// Instead, we pass the animation name for each sprite and handle all of the animation states
        /// within the prefab.
        /// </summary>
        public List<GrowthStage> GrowthStages;

        public int StartYearAfterWinter;
        public bool SurvivesWinter;

        public bool WinterProofed;

        [NonSerialized]
        public bool WasJustPlaced = true;
        public bool SlatedForDeletion;
        [NonSerialized]
        public float BarProgress;

        [NonSerialized]
        public GrowthStage CurrentGrowthStage;

        #endregion

        #region Properties

        public bool IsEffectActive => CurrentGrowthStage.ActiveEffect;

        #endregion

        #region Public Methods

        public virtual PlotResult EndTurn(FieldState field, int x, int y, SurroundingState surroundingState, float age)
        {
            WasJustPlaced = false;
            WinterProofed = surroundingState.AvailableEffects.Any(x => x.Effect == EffectType.WinterImmortal);
            if (WinterProofed)
                Debug.Log($"{Name} was just winterproofed.");
            return PlotResult.None;
        }

        public GrowthStage SetNextGrowthStage(float age)
        {
            var next = GrowthStages.FirstOrDefault(x => x.EndTurn > Math.Floor(age));

            if (next != null)
            {
                foreach (var stage in GrowthStages.Where(x => x.EndTurn > age))
                {
                    if (next.EndTurn > stage.EndTurn)
                        next = stage;
                }
            }

            CurrentGrowthStage = next;
            return next;
        }

        #endregion

        #region Nested Types

        [Serializable]
        public class GrowthStage
        {
            #region Fields

            public int EndTurn;
            public string AnimationName;
            public bool ActiveEffect;
            public int RevertToTurn = -1;

            public GameObject DetailObjectPrefab;

            #endregion
        }

        #endregion
    }
}
