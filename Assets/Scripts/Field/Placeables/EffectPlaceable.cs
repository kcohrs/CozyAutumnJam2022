﻿using AutumnJam.Utility;

using UnityEngine;

namespace AutumnJam.Field.Placeables
{
    [CreateAssetMenu(fileName = "Placeable", menuName = "CozyAutumnObjects/Effect", order = 4)]
    public class EffectPlaceable : Placeable
    {
        #region Fields

        public EffectType Effect;

        #endregion
    }
}
