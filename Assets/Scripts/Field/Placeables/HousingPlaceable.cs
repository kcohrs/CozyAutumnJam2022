﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutumnJam.Utility;

using UnityEngine;

namespace AutumnJam.Field.Placeables
{
    [CreateAssetMenu(fileName = "Placeable", menuName = "CozyAutumnObjects/Housing", order = 3)]
    public class HousingPlaceable : Placeable
    {
        #region Fields

        public float RequiredFood;
        public int VictoryPoints;

        public float AutoIncreaseFood;
        public float DefaultFoodEfficiency;

        public List<FoodEfficiency> Efficiencies;

        #endregion

        #region Properties

        public float CurrentFood { get; private set; }

        #endregion

        #region Public Methods

        public override PlotResult EndTurn(FieldState field, int x, int y, SurroundingState surroundingState, float age)
        {
            var result = base.EndTurn(field, x, y, surroundingState, age);

            var happinessIncreasers = surroundingState.AvailableEffects.Count(x => x.Effect == EffectType.AutoHappiness);

            if (happinessIncreasers > 0)
            {
                var boosters = surroundingState.AvailableEffects.Count(x => x.Effect == EffectType.AutoHappiness && x.Boosted);

                // increase food 
                CurrentFood += AutoIncreaseFood * happinessIncreasers + AutoIncreaseFood * boosters * 0.5f;
                BarProgress = Math.Clamp(CurrentFood / RequiredFood, 0, 1);
            }

            return result;
        }

        public void AddFood(FoodType type, float amount)
        {
            if (Efficiencies != null && Efficiencies.Any(x => x.Type == type))
            {
                var efficiency = Efficiencies.First(x => x.Type == type);
                CurrentFood += amount * efficiency.Efficiency;
            }
            else
                CurrentFood += amount * DefaultFoodEfficiency;

            BarProgress = Math.Clamp(CurrentFood / RequiredFood, 0, 1);
        }

        public void ResetFood()
        {
            CurrentFood = 0;
            BarProgress = Math.Clamp(CurrentFood / RequiredFood, 0, 1);
        }

        #endregion
    }
}
