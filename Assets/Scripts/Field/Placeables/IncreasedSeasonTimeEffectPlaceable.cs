﻿using System.Linq;

using AutumnJam.Utility;

using UnityEngine;

namespace AutumnJam.Field.Placeables
{
    [CreateAssetMenu(fileName = "Placeable", menuName = "CozyAutumnObjects/IncreasedSeasonEffect", order = 5)]
    public class IncreasedSeasonTimeEffectPlaceable : EffectPlaceable
    {
        #region Fields

        public int AdditionalTurns;
        private bool _activated;

        #endregion

        #region Public Methods

        public override PlotResult EndTurn(FieldState field, int x, int y, SurroundingState surroundingState, float age)
        {
            var result = base.EndTurn(field, x, y, surroundingState, age);

            if (CurrentGrowthStage.ActiveEffect && !ProgressManager.Instance.IsYearProlonged)
            {
                var boosters = surroundingState.AvailableEffects.Count(x => x.Effect == EffectType.IncreasedEffects);

                ProgressManager.Instance.AddTurnsToYear(AdditionalTurns + boosters);
                ProgressManager.Instance.IsYearProlonged = true;
            }

            return result;
        }

        #endregion
    }
}
