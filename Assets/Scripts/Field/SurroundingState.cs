﻿using System.Collections.Generic;

using AutumnJam.Utility;

namespace AutumnJam.Field
{
    public class SurroundingState
    {
        #region Fields

        public List<(EffectType Effect, int Distance, bool Boosted)> AvailableEffects;

        #endregion

        #region Constructors

        public SurroundingState()
        {
            AvailableEffects = new List<(EffectType Effect, int Distance, bool Boosted)>();
        }

        #endregion
    }
}
