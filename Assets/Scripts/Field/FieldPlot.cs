using System;
using System.Collections.Generic;
using System.Diagnostics;

using AutumnJam.Field.Placeables;
using AutumnJam.Utility;

using UnityEngine;

namespace AutumnJam.Field
{
    [DebuggerDisplay("{OccupiedBy} Plot: {_placeable} ")]
    public class FieldPlot : MonoBehaviour
    {
        #region Delegates

        public delegate void OnVariableChangeDelegate(Placeable newVal);

        #endregion

        #region Fields

        [SerializeField]
        public Placeable OccupiedBy;

        [SerializeField]
        public int PosX;

        [SerializeField]
        public int PosY;

        public bool TickedThisYear;

        [SerializeField]
        private GameObject _placeablePrefab;
        [SerializeField]
        private GameObject _placeablePreviewPrefab;

        private Placeable _occupiedBy;

        private PlaceableHandler _placeable;
        private PlaceablePreviewHandler _previewPlaceable;
        private FieldState _state;

        [SerializeField]
        private ParticleSystem _onClickParticles;
        [SerializeField]
        private ParticleSystem _placeParticles;
        private bool _adjacentToWater;

        [SerializeField]
        private MouseOverHandler _mouseOverHandler;

        #endregion

        #region Events

        public event OnVariableChangeDelegate OnOccupiedByChanged;

        #endregion

        #region Properties

        public bool IsGlobalEffect => OccupiedBy != null && OccupiedBy.Category == PlaceableCategory.GlobalEffector;

        public bool IsLocalEffect => OccupiedBy != null && OccupiedBy.Category == PlaceableCategory.LocalEffector;

        public bool IsProduction => OccupiedBy != null && OccupiedBy.Category == PlaceableCategory.Production;

        public bool IsHousing => OccupiedBy != null && OccupiedBy.Category == PlaceableCategory.Housing;

        public bool IsEmpty => OccupiedBy == null || OccupiedBy.Category == PlaceableCategory.Shovel;

        #endregion

        #region Public Methods

        public void Initialize(FieldState state, int x, int y)
        {
            _state = state;
            PosX = x;
            PosY = y;
        }

        public void Tick()
        {
            TickedThisYear = true;

            if (_placeable != null && !OccupiedBy.SlatedForDeletion)
                _placeable.Tick(_state, PosX, PosY);
        }

        public void UpdateOccupier()
        {
            if (OccupiedBy != _occupiedBy)
            {
                HandleOccupiedByChangeInternal();
                _occupiedBy = OccupiedBy;
                OnOccupiedByChanged?.Invoke(OccupiedBy);
            }
        }

        public void EndYear()
        {
            if (OccupiedBy?.BarProgress >= 1f)
            {
                PickUpManager.Instance.SpawnPlayerVictoryPointPickUp(transform.position, ((HousingPlaceable)OccupiedBy).VictoryPoints);
                OccupiedBy.BarProgress = 0f;
            }

            _placeable?.SetToWinter();

            if (_placeable != null && _occupiedBy != null && !_occupiedBy.SurvivesWinter && !_occupiedBy.WinterProofed)
                OccupiedBy = null;
        }

        public void StartYear()
        {
            if (_placeable != null && _occupiedBy != null)
            {
                if (!_occupiedBy.SurvivesWinter && !_occupiedBy.WinterProofed)
                    OccupiedBy = null;
                else
                    _placeable.ResetToPostWinter();
            }
        }

        public SurroundingState GetSurroundingState(int i)
        {
            return _placeable?.GetSurroundingState(_state, PosX, PosY, i);
        }

        public List<FieldPlot> GetAdjacentEffectorsOfType(EffectType type)
        {
            var ret = new List<FieldPlot>();

            foreach (var plot in _state.State)
            {
                if (plot.IsLocalEffect && Math.Abs(plot.PosX - PosX) <= 1 && Math.Abs(plot.PosY - PosY) <= 1)
                {
                    var effect = plot.OccupiedBy as EffectPlaceable;

                    if (!plot.TickedThisYear)
                        plot.Tick();
                    if (effect.Effect == type && effect.IsEffectActive)
                        ret.Add(plot);
                }
            }

            return ret;
        }

        #endregion

        #region Non-Public Methods

        // Start is called before the first frame update
        void Start()
        {
            _adjacentToWater = TileManager.Instance.IsAdjacentToWater(new Vector3Int(PosX, PosY));
        }

        private void OnMouseOver()
        {
            if (!TileManager.Instance.IsUnlockedTile(new Vector3Int(PosX, PosY)))
                return;

            if (ProgressManager.Instance != null && _previewPlaceable == null)
            {
                var selected = ProgressManager.Instance.GetSelectedPlaceable();

                if (selected == null)
                    return;

                if (_placeable == null)
                {
                    var placed = Instantiate(_placeablePreviewPrefab, transform);
                    _previewPlaceable = placed.GetComponent<PlaceablePreviewHandler>();
                    _previewPlaceable.PreviewPlaceable(selected);

                    if (!_adjacentToWater && selected.LimitedToWater)
                        _previewPlaceable.MarkForbidden();
                }

                // make existing thing fade out if shovel
                if (_placeable != null && selected?.Category == PlaceableCategory.Shovel)
                    _placeable.FadeOut();
            }
        }

        private void OnMouseDown()
        {
            if (!GameStateManager.Instance.IsPlaying || HudManager.Instance.IsMouseOverUI)
                return;

            var selected = ProgressManager.Instance.GetSelectedPlaceable();

            if (!_adjacentToWater && selected != null && selected.LimitedToWater)
            {
                AudioManager.Instance.PlaySound(AudioManager.Sound.Error);
                return;
            }

            AudioManager.Instance.PlaySound(AudioManager.Sound.Click);
            var target = Input.mousePosition;
            target = Camera.main.ScreenToWorldPoint(target);
            _onClickParticles.transform.position = new Vector3(target.x, target.y, _onClickParticles.transform.position.z);
            _onClickParticles.Play();

            if (!TileManager.Instance.IsUnlockedTile(new Vector3Int(PosX, PosY)))
            {
                if (selected != null)
                    ProgressManager.Instance.ResetSelectedPlaceable();

                return;
            }

            if (selected?.Category == PlaceableCategory.Shovel)
            {
                if (OccupiedBy != null && OccupiedBy.WasJustPlaced)
                    ProgressManager.Instance.RefundPlaceable(OccupiedBy);
                // delete if anything exists
                OccupiedBy = null;
                return;
            }

            // do not plant on top of each other
            if (_previewPlaceable != null && OccupiedBy == null)
            {
                var success = ProgressManager.Instance.PurchasePlaceable(_previewPlaceable.Info);

                if (success)
                {
                    var placeable = _previewPlaceable.Info;
                    OccupiedBy = placeable;
                    AudioManager.Instance.PlaySound(AudioManager.Sound.Planted);
                    _placeParticles.Play();
                    Destroy(_previewPlaceable.gameObject);
                }
            }
        }

        private void OnMouseExit()
        {
            if (!TileManager.Instance.IsUnlockedTile(new Vector3Int(PosX, PosY)))
                return;

            var selected = ProgressManager.Instance.GetSelectedPlaceable();

            if (_placeable != null && selected?.Category == PlaceableCategory.Shovel)
                _placeable.ResetFadeOut();

            if (_previewPlaceable != null)
                Destroy(_previewPlaceable.gameObject);
        }

        private void Update()
        {
            UpdateOccupier();
        }

        private void HandleOccupiedByChangeInternal()
        {
            // instantiate prefab from placeable 
            if (OccupiedBy == null || OccupiedBy.Category == PlaceableCategory.Shovel)
            {
                if (_placeable != null)
                    Destroy(_placeable.gameObject);
                OccupiedBy = null;
                _placeable = null;
                _mouseOverHandler.ResetPlaceable();
            }
            else
            {
                OccupiedBy = Instantiate(OccupiedBy);
                var placed = Instantiate(_placeablePrefab, transform);
                _placeable = placed.GetComponent<PlaceableHandler>();
                _placeable.SetupPlaceable(OccupiedBy);
                _mouseOverHandler.SetupPlaceable(OccupiedBy);
            }
        }

        #endregion
    }

    public enum PlaceableCategory
    {
        Shovel,
        Production,
        Housing,
        LocalEffector,
        GlobalEffector
    }
}
