using AutumnJam.Utility;

using UnityEngine;

namespace AutumnJam.Field
{
    public class MousePickup : MonoBehaviour
    {
        #region Fields

        public Sprite Sprite;

        public Food Food;
        public int VictoryPoints;

        private float _pickUpDistance = 0.5f;
        private float _followDistance = 2f;
        private float _yLimit;
        private Rigidbody2D _rigidbody;
        private bool _followMouse;
        private float _moveSpeed;

        #endregion

        #region Public Methods

        public void AddImpulse(float positionY)
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _rigidbody.AddForce(new Vector2(Random.Range(-2f, 2f), 2), ForceMode2D.Impulse);
            _yLimit = positionY + Random.Range(-0.4f, 0.4f);
        }

        public void SetUpVictoryPoints(float positionY, int value)
        {
            VictoryPoints = value;
            AddImpulse(positionY);
        }

        public void SetUpFood(float positionY, FoodType foodName, float value)
        {
            Food = new Food(type: foodName, amount: value);
            AddImpulse(positionY);
        }

        #endregion

        #region Non-Public Methods

        // Update is called once per frame
        void FixedUpdate()
        {
            if (!_followMouse && _rigidbody != null && transform.position.y <= _yLimit)
                _rigidbody.simulated = false;

            var target = Input.mousePosition;
            target = Camera.main.ScreenToWorldPoint(target);

            if (ProgressManager.Instance.IsEndingYear || ProgressManager.Instance.IsEndingLastYear)
            {
                _followMouse = true;
                if (Food?.Amount > 0)
                    target = ProgressManager.Instance.FoodHudLocation;
                else
                    target = ProgressManager.Instance.VictoryPointsHudLocation;
            }

            if (ProgressManager.Instance.IsEndingYear)
                ProgressManager.Instance.TrackLeftoverFood(this);

            if (!_followMouse && Vector2.Distance(target, transform.position) < _followDistance)
                _followMouse = true;

            if (_followMouse)
            {
                // despawn if close to mouse
                if (Vector2.Distance(target, transform.position) < _pickUpDistance || _moveSpeed > 0.5f)
                {
                    ProgressManager.Instance.AddPickup(Food, VictoryPoints);

                    Destroy(gameObject);
                }

                // make pickup lerp towards the mouse cursor
                transform.position = Vector2.MoveTowards(transform.position, target, _moveSpeed);
                _moveSpeed += 0.01f;
            }
        }

        #endregion
    }
}
