using System.Collections.Generic;

using AutumnJam.Field.Placeables;

using UnityEngine;

namespace AutumnJam.Field
{
    [ExecuteInEditMode]
    public class FieldState : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        public int SizeX = 10;
        [SerializeField]
        public int SizeY = 8;

        [SerializeField]
        public FieldPlot[,] State;

        [SerializeField]
        private Grid GridComponent;

        [SerializeField]
        private GameObject _basePlaceablePrefab;
        [SerializeField]
        private GameObject _placeableContainer;

        #endregion

        #region Public Methods

        public Vector3 GetPositionForField(int x, int y)
        {
            // we assume middle of field is at 0,0,0
            var vec = new Vector3(x * GridComponent.cellSize.x - SizeX * GridComponent.cellSize.x / 2f,
                                  y * GridComponent.cellSize.y - SizeY * GridComponent.cellSize.y / 2f, -1f);

            // low index in front, so substract index * value to make front -1, and end still >0
            var increment = 1f / (SizeY + 1);
            vec.z += y * increment;

            // include adjustment for uneven field counts
            vec.x += SizeX % 2 == 0 ? GridComponent.cellSize.x / 2 : 0f;
            vec.y += SizeY % 2 == 0 ? GridComponent.cellSize.y / 2 : 0f;
            return vec;
        }

        public void HandleEndOfTurn()
        {
            // update colors 
            // do grow ticks

            foreach (var pos in State)
                pos.TickedThisYear = false;

            foreach (var pos in State)
            {
                if (!pos.TickedThisYear)
                    pos.Tick();

                if (pos.OccupiedBy != null && pos.OccupiedBy.SlatedForDeletion)
                    pos.OccupiedBy = null;
            }
        }

        public void HandleStartOfYear()
        {
            foreach (var pos in State)
                pos.StartYear();
        }

        public void HandleEndOfYear()
        {
            foreach (var pos in State)
                pos.EndYear();
        }

        public void ForcePlantAtLocation(int x, int y, Placeable placeable, int age)
        {
            var plot = State[x, y];

            if (plot != null)
            {
                plot.OccupiedBy = placeable;
                plot.UpdateOccupier();
                if (plot.OccupiedBy != null)
                    plot.OccupiedBy.WasJustPlaced = false;
                for (var i = 0; i < age; i++)
                    plot.Tick();
            }
        }

        #endregion

        #region Non-Public Methods

        private void Awake()
        {
            // regenerate fields set during edit mode
            var children = new List<GameObject>();

            foreach (Transform child in _placeableContainer.transform)
                children.Add(child.gameObject);
            foreach (var child in children)
                DestroyImmediate(child);

            GenerateField();

#if UNITY_EDITOR
            _lastWidth = GridComponent.cellSize.x;
            _lastHeight = GridComponent.cellSize.y;
            _lastX = SizeX;
            _lastY = SizeY;
#endif
        }

        private void Start()
        {
        }

        private void OnMouseDown()
        {
            if (ProgressManager.Instance != null)
            {
                var selected = ProgressManager.Instance.GetSelectedPlaceable();
                if (selected != null)
                    ProgressManager.Instance.ResetSelectedPlaceable();
            }
        }
#if UNITY_EDITOR
        private void Update()
        {
            // helpers during design
            if (GridComponent.cellSize.x != _lastWidth || _lastHeight != GridComponent.cellSize.y || _lastX != SizeX || _lastY != SizeY)
            {
                // avoid iterator to not break loop
                var children = new List<GameObject>();

                foreach (Transform child in _placeableContainer.transform)
                    children.Add(child.gameObject);
                foreach (var child in children)
                    DestroyImmediate(child);

                GenerateField();

                Debug.LogWarning("Remember to update the tilemap offset (Field > BackgroundTilemap)!");

                _lastWidth = GridComponent.cellSize.x;
                _lastHeight = GridComponent.cellSize.y;
                _lastX = SizeX;
                _lastY = SizeY;
            }
        }
#endif
        private void GenerateField()
        {
            State = new FieldPlot[SizeX, SizeY];

            for (var i = 0; i < SizeX; i++)
            {
                for (var j = 0; j < SizeY; j++)
                {
                    var pos = GetPositionForField(i, j);
                    var obj = Instantiate(_basePlaceablePrefab, pos, Quaternion.identity, _placeableContainer.transform);
                    State[i, j] = obj.GetComponent<FieldPlot>();
                    State[i, j].Initialize(this, i, j);
                }
            }
            // if need be we could spread this out over multiple frames
        }

        #endregion

#if UNITY_EDITOR
        private float _lastHeight;
        private float _lastWidth;
        private int _lastX;
        private int _lastY;

#endif
    }
}
