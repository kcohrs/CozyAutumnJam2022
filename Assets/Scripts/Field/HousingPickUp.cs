using System;

using AutumnJam.Field.Placeables;
using AutumnJam.Utility;

using UnityEngine;

using Random = UnityEngine.Random;

namespace AutumnJam.Field
{
    public class HousingPickUp : MonoBehaviour
    {
        #region Fields

        private Rigidbody2D _rigidbody;
        private float _yLimit;
        private FieldPlot _target;
        private float _absorbTime;
        private bool _absorbing;
        private float _pickUpDistance = 0.5f;
        private Food _food;
        private float _moveSpeed;

        #endregion

        #region Public Methods

        public void AddImpulse(float positionY)
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _rigidbody.AddForce(new Vector2(Random.Range(-2f, 2f), 2), ForceMode2D.Impulse);
            _yLimit = positionY + Random.Range(-0.4f, 0.4f);
        }

        public void SetUpTarget(float positionY, FoodType type, float amount, FieldPlot targetHousing)
        {
            AddImpulse(positionY);

            _food = new Food(type, amount);
            _target = targetHousing;
            _absorbTime = Time.time + 0.5f + Random.Range(0, 0.3f);
        }

        #endregion

        #region Non-Public Methods

        void Update()
        {
            if (_rigidbody != null && transform.position.y <= _yLimit)
                _rigidbody.simulated = false;

            if (!_absorbing && Time.time > _absorbTime)
                _absorbing = true;

            if (ProgressManager.Instance.IsEndingYear)
            {
                var housing = _target.OccupiedBy as HousingPlaceable;
                var remaining = housing.RequiredFood - housing.CurrentFood;
                var houseFood = Math.Min(remaining, _food.Amount);

                if (houseFood > 0)
                    housing.AddFood(_food.Type, _food.Amount);

                if (remaining < _food.Amount && !ProgressManager.Instance.IsEndingYear)
                {
                    // less remaining than given
                    PickUpManager.Instance.SpawnPlayerFoodPickUp(_target.transform.position, _food.Amount - remaining, _food.Type);
                }

                Destroy(gameObject);
            }

            if (_absorbing)
            {
                if (Vector2.Distance(_target.transform.position, transform.position) < _pickUpDistance)
                {
                    var housing = _target.OccupiedBy as HousingPlaceable;

                    var remaining = housing.RequiredFood - housing.CurrentFood;
                    var houseFood = Math.Min(remaining, _food.Amount);

                    if (GameStateManager.Instance.IsPlaying)
                    {
                        if (houseFood > 0)
                            housing.AddFood(_food.Type, _food.Amount);

                        if (remaining < _food.Amount && !ProgressManager.Instance.IsEndingYear)
                        {
                            // less remaining than given
                            PickUpManager.Instance.SpawnPlayerFoodPickUp(_target.transform.position, _food.Amount - remaining, _food.Type);
                        }
                    }

                    Destroy(gameObject);
                }

                // make pickup lerp towards the mouse cursor

                transform.position = Vector2.Lerp(transform.position, _target.transform.position, _moveSpeed);
                _moveSpeed += 0.01f;
            }
        }

        #endregion
    }
}
