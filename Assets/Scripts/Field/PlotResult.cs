﻿using AutumnJam.Utility;

namespace AutumnJam.Field
{
    public class PlotResult
    {
        #region Fields

        public Food Food;
        public int VictoryPoints;

        #endregion

        #region Constructors

        public PlotResult(Food food, int victoryPoints)
        {
            Food = food;
            VictoryPoints = victoryPoints;
        }

        #endregion

        #region Properties

        public static PlotResult None => new PlotResult(new Food(FoodType.None, 0), 0);

        #endregion
    }
}
