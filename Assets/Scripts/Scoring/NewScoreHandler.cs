using TMPro;

using UnityEngine;
using UnityEngine.UI;
using LootLocker.Requests;
using System;
using System.Collections.Generic;
using System.Collections;

namespace AutumnJam.Scoring
{
    public class NewScoreHandler : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        private TextMeshProUGUI _points;

        [SerializeField]
        private TMP_InputField _inputField;
        [SerializeField]
        private GameObject _inputFieldArea;
        [SerializeField]
        private TextMeshProUGUI _enteredName;
        [SerializeField]
        private Button _saveButton;
        private int _score;

        #endregion

        #region Public Methods

        public void SetupEntry(int newScore)
        {
            _score = newScore;
            _points.text = $"{newScore}";
        }

        
        #endregion
    }
}
