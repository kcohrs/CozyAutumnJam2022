using System;
using System.Collections.Generic;

namespace AutumnJam.Scoring
{
    [Serializable]
    public class HighScoreEntry
    {
        #region Fields

        public string Name;
        public int Score;

        #endregion

        #region Constructors

        public HighScoreEntry(string name, int score)
        {
            Name = name;
            Score = score;
        }

        #endregion
    }

    [Serializable]
    public class HighScores
    {
        #region Fields

        public List<HighScoreEntry> Entries;

        #endregion

        #region Constructors

        public HighScores()
        {
            Entries = new List<HighScoreEntry>();
        }

        #endregion
    }
}
