using TMPro;

using UnityEngine;

namespace AutumnJam.Scoring
{
    public class HighScoreHandler : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        private TextMeshProUGUI _name;
        [SerializeField]
        private TextMeshProUGUI _points;

        #endregion

        #region Public Methods

        public void SetupEntry(HighScoreEntry score)
        {
            _name.text = score.Name;
            _points.text = $"{score.Score}";
        }      

        #endregion
    }
}
