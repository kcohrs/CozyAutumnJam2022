using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using AutumnJam.Utility;

using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

using static iTween;

namespace AutumnJam
{
    public class AudioManager : MonoBehaviour
    {
        #region Enums

        public enum Sound
        {
            None,
            Click, // prolly remove me?
            FoodProduced,
            Error,
            Planted,
            FoodGained,
            VictoryPointGained,
            Hover,
            ClickUi,
            EndYear
        }

        public enum BackgroundMusic
        {
            None,
            Menu, // glimpse of eternity
            Gameplay1, // Cerf
            Gameplay2, // ?
            Tutorial, // ?
            Credits, // pure water, if we add a rain effect and colorgrade
            Highscore // Interplanetary Forest ez.
        }

        #endregion

        #region Constants

        public const string EffectsName = "Effects";
        public const string MasterName = "Master";
        public const string MusicName = "Music";

        #endregion

        #region Fields

        [SerializeField]
        public List<AudioEffectData> EffectsLibrary;
        [SerializeField]
        public List<AudioMusicData> MusicLibrary;

        [SerializeField]
        private AudioMixer _audioMixer;
        [SerializeField]
        private AudioSource _musicSource;
        [SerializeField]
        private GameObject _dynamicAudioPrefab;

        [SerializeField]
        private Slider _masterSlider;
        [SerializeField]
        private Slider _effectsSlider;
        [SerializeField]
        private Slider _musicSlider;
        [SerializeField]
        private Slider _playMasterSlider;
        [SerializeField]
        private Slider _playEffectsSlider;
        [SerializeField]
        private Slider _playMusicSlider;

        private List<SoundEntry> _sounds = new List<SoundEntry>();
        private double _minimumGap = 0.1f;

        #endregion

        #region Constructors

        public AudioManager()
        {
            Instance = this;
        }

        #endregion

        #region Properties

        public static AudioManager Instance { get; private set; }

        #endregion

        #region Public Methods

        public void SetVolume(float volume, string channel)
        {
            _audioMixer.SetFloat(channel, volume > 0 ? Mathf.Log(volume) * 20f : -80); // falloff fix
        }

        public void PlayBackgroundMusic(BackgroundMusic music, bool forced = false)
        {
            // if anything is playing, fade it out, then fade in new after it ended
            if (_musicSource != null)
            {
                if (forced)
                {
                    var clip = MusicLibrary.FirstOrDefault(x => x.Music == music);
                    _musicSource.clip = clip.File;
                    _musicSource.Play();
                }
                else
                {
                    AudioTo(_musicSource.gameObject, Hash("volume", 0f, "time", 2f, "easetype", EaseType.easeOutExpo));
                    StartCoroutine(FadeMusicIn(music));
                }
            }
        }

        public IEnumerator FadeMusicIn(BackgroundMusic music)
        {
            yield return new WaitForSeconds(2f);

            var clip = MusicLibrary.FirstOrDefault(x => x.Music == music);
            _musicSource.clip = clip.File;
            _musicSource.Play();
            AudioTo(_musicSource.gameObject, Hash("volume", 1f, "time", 4f, "easetype", EaseType.easeInExpo));
        }

        public void PlaySound(Sound sound)
        {
            // check when last instance of sound was played, if minimum time elapsed play another
            var entry = _sounds.FirstOrDefault(x => x.Sound == sound);

            if ((entry != null && entry.Time + _minimumGap < Time.time) || entry == null)
            {
                if (entry != null)
                {
                    Destroy(entry.Player);
                    _sounds.RemoveAll(x => x.Sound == sound);
                }

                var clip = EffectsLibrary.FirstOrDefault(x => x.Sound == sound);

                // sound does not exist
                if (clip == null)
                    return;

                var audioObject = Instantiate(_dynamicAudioPrefab, transform);
                var audioEffect = audioObject.GetComponent<AudioEffect>();
                audioEffect.Setup(clip.File);
                _sounds.Add(new SoundEntry(sound, Time.time, audioObject));
            }
        }

        #endregion

        #region Non-Public Methods

        private void Start()
        {
            _masterSlider?.onValueChanged.AddListener(value =>
            {
                SetVolume(value, MasterName);
            });
            _masterSlider.value = 1f;
            _effectsSlider?.onValueChanged.AddListener(value =>
            {
                SetVolume(value, EffectsName);
            });
            _effectsSlider.value = 1f;
            _musicSlider?.onValueChanged.AddListener(value =>
            {
                SetVolume(value, MusicName);
            });
            _musicSlider.value = 1f;
            _playMasterSlider?.onValueChanged.AddListener(value =>
            {
                SetVolume(value, MasterName);
            });
            _playMasterSlider.value = 1f;
            _playEffectsSlider?.onValueChanged.AddListener(value =>
            {
                SetVolume(value, EffectsName);
            });
            _playEffectsSlider.value = 1f;
            _playMusicSlider?.onValueChanged.AddListener(value =>
            {
                SetVolume(value, MusicName);
            });
            _playMusicSlider.value = 1f;
        }

        #endregion

        #region Nested Types

        public class SoundEntry
        {
            #region Fields

            public float Time;
            public Sound Sound;
            public GameObject Player;

            #endregion

            #region Constructors

            public SoundEntry(Sound sound, float time, GameObject player)
            {
                Sound = sound;
                Time = time;
                Player = player;
            }

            #endregion
        }

        [Serializable]
        public class AudioMusicData
        {
            #region Fields

            public AudioClip File;
            public BackgroundMusic Music;

            #endregion
        }

        [Serializable]
        public class AudioEffectData
        {
            #region Fields

            public AudioClip File;
            public Sound Sound;

            #endregion
        }

        #endregion
    }
}
