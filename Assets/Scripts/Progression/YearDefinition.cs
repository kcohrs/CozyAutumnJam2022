using System.Collections.Generic;

using AutumnJam.Field.Placeables;

using UnityEngine;

namespace AutumnJam.Progression
{
    [CreateAssetMenu(fileName = "Data", menuName = "CozyAutumnObjects/Year", order = 2)]
    public class YearDefinition : ScriptableObject
    {
        #region Fields

        public int Turns;
        public int Year;
        public List<Placeable> Unlocks;

        public bool IsLastYear;

        public List<Vector2Int> RegionUnlocks;

        #endregion
    }
}
