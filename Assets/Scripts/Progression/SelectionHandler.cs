using AutumnJam.Field.Placeables;

using TMPro;

using UnityEngine;
using UnityEngine.UI;

namespace AutumnJam.Progression
{
    public class SelectionHandler : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        private TextMeshProUGUI _priceText;
        [SerializeField]
        private Image _icon;
        [SerializeField]
        private Image _locked;
        [SerializeField]
        private Image _selected;
        private Button _button;

        [SerializeField]
        private Placeable _placeable;

        #endregion

        #region Public Methods

        public void OnClick()
        {
            if (ProgressManager.Instance.IsPlaceableUnlocked(_placeable) && ProgressManager.Instance.GetStoredFood() >= _placeable.Price)
                ProgressManager.Instance.SelectPlaceable(_placeable);
        }

        public void UpdateState()
        {
            _button ??= GetComponent<Button>();

            var unlocked = ProgressManager.Instance.IsPlaceableUnlocked(_placeable);

            if (unlocked && ProgressManager.Instance.GetStoredFood() >= _placeable.Price)
            {
                _button.interactable = true;
                _icon.color = Color.white;
            }

            else
            {
                _button.interactable = false;
                _icon.color = new Color(0.305f, 0.293f, 0.297f);
            }

            if (unlocked)
                _locked.enabled = false;
            else
                _locked.enabled = true;
            var selected = ProgressManager.Instance.GetSelectedPlaceable();
            if (selected != null && selected.Name == _placeable.Name)
                _selected.enabled = true;
            else
                _selected.enabled = false;
        }

        #endregion

        #region Non-Public Methods

        private void Start()
        {
            if (_placeable != null)
            {
                _priceText.text = $"{_placeable.Price}";
                _icon.sprite = _placeable.ToolTipSprite;

                HudManager.Instance.RegisterSelectionHandler(this);
                UpdateState();
            }
        }

        #endregion
    }
}
