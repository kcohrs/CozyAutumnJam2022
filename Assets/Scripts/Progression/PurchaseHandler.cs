using AutumnJam.Field.Placeables;

using TMPro;

using UnityEngine;
using UnityEngine.UI;

namespace AutumnJam.Progression
{
    [ExecuteInEditMode]
    public class PurchaseHandler : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        private TextMeshProUGUI _priceText;
        [SerializeField]
        private Image _icon;
        [SerializeField]
        private Image _checkbox;
        private Button _button;

        [SerializeField]
        private Placeable _placeable;

        #endregion

        #region Public Methods

        public void OnClick()
        {
            ProgressManager.Instance.UnlockPlaceable(_placeable);
        }

        public void UpdateState()
        {
            _button ??= GetComponent<Button>();

            var unlocked = ProgressManager.Instance.IsPlaceableUnlocked(_placeable);

            if (ProgressManager.Instance.GetStoredFood() >= _placeable.UnlockPrice && ProgressManager.Instance.Year >= _placeable.UnlockYear && !unlocked)
            {
                _button.interactable = true;
                _icon.color = Color.white;
            }

            else
            {
                _button.interactable = false;
                _icon.color = new Color(0.305f, 0.293f, 0.297f);
            }

            if (unlocked)
            {
                _checkbox.enabled = true;
                _priceText.text = "";
            }
            else
            {
                _checkbox.enabled = false;
                _priceText.text = $"{_placeable.UnlockPrice}";
            }
        }

        #endregion

        #region Non-Public Methods

        private void Start()
        {
            if (_placeable != null)
            {
                _priceText.text = $"{_placeable.UnlockPrice}";
                _icon.sprite = _placeable.ToolTipSprite;
            }

            HudManager.Instance.RegisterPurchaseHandler(this);
        }

        #endregion
    }
}
