using System.Collections.Generic;

using TMPro;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace AutumnJam
{
    public class Tutorial : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
    {
        #region Fields

        public bool IsTutorialEnabled;
        public Transform target;
        public bool shouldReturn;

        [SerializeField]
        private int _state;
        [SerializeField]
        private TextMeshProUGUI _hintText;
        [SerializeField]
        private Button _backButton;
        [SerializeField]
        private Button _forwardButton;
        [SerializeField]
        private GameObject _tutorialCanvas;

        [TextArea(0, 100)]
        [SerializeField]
        private List<string> _hints;
        private bool isMouseDown;
        private Vector3 startMousePosition;
        private Vector3 startPosition;
        private bool _tutorialStarted;

        #endregion

        #region Public Methods

        public void AdvanceTutorial()
        {
            if (!IsTutorialEnabled)
                return;

            _state++;

            HandleTutorialState();
        }

        public void StepBackTutorial()
        {
            if (!IsTutorialEnabled)
                return;

            _state--;

            HandleTutorialState();
        }

        public void SetTutorialEnabled(bool activated)
        {
            IsTutorialEnabled = activated;
        }

        public void AbortTutorial()
        {
            _tutorialCanvas.SetActive(false);
            IsTutorialEnabled = false;
            _tutorialStarted = false;
            _state = 0;
        }

        public void StartTutorial()
        {
            _state = 0;
            if (!IsTutorialEnabled)
                return;

            _tutorialStarted = true;
            _tutorialCanvas.SetActive(true);
            HandleTutorialState();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            isMouseDown = true;

            startPosition = target.position;
            startMousePosition = Input.mousePosition;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            isMouseDown = false;

            if (shouldReturn)
                target.position = startPosition;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            HudManager.Instance.IsMouseOverUI = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            HudManager.Instance.IsMouseOverUI = false;
        }

        #endregion

        #region Non-Public Methods

        private void HandleTutorialState()
        {
            if (!IsTutorialEnabled)
                return;

            if (_state == _hints.Count)
            {
                AbortTutorial();
                return;
            }

            if (_hints.Count > _state && _state >= 0)
                _hintText.text = _hints[_state];

            if (_state == 0)
                _backButton.gameObject.SetActive(false);
            else
                _backButton.gameObject.SetActive(true);

            if (_state == _hints.Count - 1)
            {
                var text = _forwardButton.GetComponentInChildren<TextMeshProUGUI>();
                text.text = "Finish";
            }
            else
            {
                var text = _forwardButton.GetComponentInChildren<TextMeshProUGUI>();
                text.text = "Next";
            }
        }

        private void Awake()
        {
            IsTutorialEnabled = true;
            _tutorialCanvas.SetActive(false);
        }

        void Update()
        {
            if (_tutorialCanvas.activeInHierarchy && (ProgressManager.Instance.IsEndingYear || !GameStateManager.Instance.IsPlaying))
                _tutorialCanvas.SetActive(false);

            if (_tutorialStarted && !_tutorialCanvas.activeInHierarchy && !ProgressManager.Instance.IsEndingYear && GameStateManager.Instance.IsPlaying)
                _tutorialCanvas.SetActive(true);

            if (isMouseDown)
            {
                var currentPosition = Input.mousePosition;
                var diff = currentPosition - startMousePosition;
                var pos = startPosition + diff;
                target.position = pos;
            }
        }

        #endregion
    }
}
