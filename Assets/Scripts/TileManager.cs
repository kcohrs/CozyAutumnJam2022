using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Tilemaps;

namespace AutumnJam
{
    public class TileManager : MonoBehaviour
    {
        #region Fields

        public static TileManager Instance;

        [SerializeField]
        private Tilemap _tiles;
        [SerializeField]
        private Tilemap _detailTiles;
        [SerializeField]
        private Tilemap _lockedDetailTiles;

        [SerializeField]
        private List<TileBase> _disabledtiles;
        [SerializeField]
        private List<TileBase> _enabledtiles;
        [SerializeField]
        private List<TileBase> _watertiles;
        [SerializeField]
        private List<TileBase> _detailDisabledTiles;
        [SerializeField]
        private List<TileBase> _winterTiles;
        [SerializeField]
        private bool _debugDetails;

        private List<( TileBase Autumn, TileBase Winter)> _winterMatch;
        private List<( TileBase Autumn, TileBase Winter)> _accentWinterMatch;

        [SerializeField]
        private GameObject _snowParticles;
        [SerializeField]
        private GameObject _leavesParticles;

        #endregion

        #region Constructors

        public TileManager()
        {
            Instance = this;
            _winterMatch = new List<(TileBase Autumn, TileBase Winter)>();
            _accentWinterMatch = new List<(TileBase Autumn, TileBase Winter)>();
        }

        #endregion

        #region Public Methods

        public void SetAmbientColor(Color color)
        {
            _tiles.color = color;
            _detailTiles.color = color;
            _lockedDetailTiles.color = color;
            // here we could do special considerations to only swithc colors for certain things 
        }

        public void UnlockRegions(List<Vector2Int> tiles)
        {
            foreach (Vector3Int tile in tiles)
                UnlockRegion(tile);
        }

        public void UnlockRegion(Vector3Int tile)
        {
            if (IsPlaceableTile(_tiles.GetTile(tile)))
            {
                _tiles.SetTile(tile, _enabledtiles[Random.Range(0, _enabledtiles.Count)]);

                for (var x = 0; x < 3; x++)
                {
                    for (var y = 0; y < 3; y++)
                    {
                        var pos = new Vector3Int(tile.x + x - 1, tile.y + y - 1);
                        UpdateTileDetails(pos, true);
                    }
                }
            }
        }

        public bool IsUnlockedTile(Vector3Int pos)
        {
            var tile = _tiles.GetTile(pos);

            if (tile == null)
                return false;

            foreach (var t in _enabledtiles)
            {
                if (tile.name == t.name)
                    return true;
            }

            return false;
        }

        public void GenerateDetails()
        {
            // called only at start of game, so might as well
            for (var x = -20; x < 30; x++)
            {
                for (var y = -20; y < 20; y++)
                {
                    var pos = new Vector3Int(x, y);
                    UpdateTileDetails(pos, true);
                }
            }
        }

        public IEnumerator LockAllTiles()
        {
            Debug.Log("Locking all tiles.");
            ClearDetails();

            for (var x = 0; x < 12; x++)
            {
                for (var y = 0; y < 6; y++)
                {
                    var pos = new Vector3Int(x, y);
                    if (IsPlaceableTile(_tiles.GetTile(pos)))
                        _tiles.SetTile(pos, _disabledtiles[Random.Range(0, _disabledtiles.Count)]);
                }
            }

            yield return 0;

            GenerateDetails();

            for (var x = -2; x < 14; x++)
            {
                for (var y = -2; y < 8; y++)
                {
                    var pos = new Vector3Int(x, y);
                    UpdateTileDetails(pos, !IsPlaceableTile(_tiles.GetTile(pos)));
                }
            }
        }

        public void UnlockAllTiles()
        {
            for (var x = 0; x < 12; x++)
            {
                for (var y = 0; y < 6; y++)
                {
                    var pos = new Vector3Int(x, y);
                    if (IsPlaceableTile(_tiles.GetTile(pos)))
                        _tiles.SetTile(pos, _enabledtiles[Random.Range(0, _enabledtiles.Count)]);
                }
            }

            for (var x = -2; x < 14; x++)
            {
                for (var y = -2; y < 8; y++)
                {
                    var pos = new Vector3Int(x, y);
                    if (!IsPlaceableTile(_tiles.GetTile(pos)))
                        UpdateTileDetails(pos, true);
                }
            }
        }

        public bool IsAdjacentToWater(Vector3Int pos)
        {
            for (var x = -1; x < 2; x++)
            {
                for (var y = -1; y < 2; y++)
                {
                    var tile = _tiles.GetTile(new Vector3Int(pos.x + x, pos.y + y));
                    if (IsWaterTile(tile))
                        return true;
                }
            }

            return false;
        }

        public bool IsPlaceableTile(Vector3Int pos)
        {
            return IsPlaceableTile(_tiles.GetTile(pos));
        }

        public bool IsPlaceableTile(TileBase tile)
        {
            if (tile == null)
                return false;

            foreach (var t in _enabledtiles)
            {
                if (tile.name == t.name)
                    return true;
            }

            foreach (var t in _disabledtiles)
            {
                if (tile.name == t.name)
                    return true;
            }

            return false;
        }

        public void ToggleWinter(bool active)
        {
            for (var x = -20; x < 30; x++)
            {
                for (var y = -20; y < 20; y++)
                {
                    var pos = new Vector3Int(x, y);
                    var tile = _tiles.GetTile(pos);
                    var accentTile = _detailTiles.GetTile(pos);
                    if (tile == null)
                        continue;

                    var match = _winterMatch.FirstOrDefault(x => x.Winter.name == tile.name || x.Autumn.name == tile.name);

                    if (active)
                    {
                        if (accentTile != null)
                        {
                            var accentMatch = _accentWinterMatch.FirstOrDefault(x => x.Winter.name == accentTile.name);
                            if (accentMatch.Winter != null)
                                _detailTiles.SetTile(pos, accentMatch.Winter);

                            var accentTileName = accentTile.name;
                            var accentWinterTile = _winterTiles.FirstOrDefault(x => x.name == accentTileName || x.name == accentTileName + " 1");

                            if (accentWinterTile == null)
                                Debug.LogWarning($"No winter match for accent tile {accentTileName}");
                            else
                            {
                                if (!_accentWinterMatch.Any(x => x.Autumn.name == accentWinterTile.name))
                                    _accentWinterMatch.Add((accentTile, accentWinterTile));
                                _detailTiles.SetTile(pos, accentWinterTile);
                            }
                        }

                        if (match.Winter != null)
                        {
                            _tiles.SetTile(pos, match.Winter);
                            continue;
                        }

                        var tileName = tile.name;
                        var winterTile = _winterTiles.FirstOrDefault(x => x.name == tileName || x.name == tileName + " 1");

                        if (winterTile == null)
                        {
                            if (tileName != null && tileName.Contains("Water"))
                                continue;

                            Debug.LogWarning($"No winter match for tile {tileName}");
                        }
                        else
                        {
                            if (!_winterMatch.Any(x => x.Autumn.name == tile.name))
                                _winterMatch.Add((tile, winterTile));
                            _tiles.SetTile(pos, winterTile);
                        }
                    }
                    else
                    {
                        if (match.Autumn != null)
                            _tiles.SetTile(pos, match.Autumn);

                        if (accentTile != null)
                        {
                            var accentMatch = _accentWinterMatch.FirstOrDefault(x => x.Winter.name == accentTile.name);
                            if (accentMatch.Autumn != null)
                                _detailTiles.SetTile(pos, accentMatch.Autumn);
                        }
                    }
                }
            }

            if (active)
            {
                _snowParticles.SetActive(true);
                _leavesParticles.SetActive(false);
            }
            else
            {
                _snowParticles.SetActive(false);
                _leavesParticles.SetActive(true);
            }
        }

        #endregion

        #region Non-Public Methods

        private void ClearDetails()
        {
            for (var x = -2; x < 14; x++)
            {
                for (var y = -2; y < 8; y++)
                {
                    var pos = new Vector3Int(x, y);
                    _detailTiles.SetTile(pos, null);
                }
            }
        }

        private bool IsLockedTile(Vector3Int pos)
        {
            var tile = _tiles.GetTile(pos);

            if (tile == null)
                return false;

            foreach (var t in _disabledtiles)
            {
                if (tile.name == t.name)
                    return true;
            }

            return false;
        }

        private void UpdateTileDetails(Vector3Int tile, bool outside = false)
        {
            if (!outside || IsLockedTile(tile))
                return;

            var locked = new bool[3, 3];

            for (var x = 0; x < 3; x++)
            {
                for (var y = 0; y < 3; y++)
                {
                    if (!IsUnlockedTile(new Vector3Int(tile.x + x - 1, tile.y + y - 1)))
                        locked[x, y] = true;

                    if (!IsPlaceableTile(new Vector3Int(tile.x + x - 1, tile.y + y - 1)))
                    {
                        // side of playing field
                        locked[x, y] = false;
                    }
                }
            }

            // if case hell because we cant really switch case this
            // we could bitmask to get a corresponding detail tile, but tbh thats more effort than
            // its worth for this "minimal" tiling system
            if (!locked[1, 0] && !locked[0, 1] && !locked[2, 1] && !locked[1, 2])
            {
                // 000
                // 000
                // 000
                _detailTiles.SetTile(tile, null);
            }

            if (locked[1, 0] && !locked[0, 1] && !locked[2, 1] && !locked[1, 2])
            {
                // 000
                // 000
                // XXX
                var index = 0;
                if (_detailDisabledTiles.Count > index)
                    _detailTiles.SetTile(tile, _detailDisabledTiles[index]);
                if (_debugDetails)
                    Debug.Log($"Setting bottom of {tile}");
            }

            if (!locked[1, 0] && locked[0, 1] && !locked[2, 1] && !locked[1, 2])
            {
                // X00
                // X00
                // X00
                var index = 1;
                if (_detailDisabledTiles.Count > index)
                    _detailTiles.SetTile(tile, _detailDisabledTiles[index]);
                if (_debugDetails)
                    Debug.Log($"Setting left of {tile}");
            }

            if (!locked[1, 0] && !locked[0, 1] && locked[2, 1] && !locked[1, 2])
            {
                // 00X
                // 00X
                // 00X
                var index = 2;
                if (_detailDisabledTiles.Count > index)
                    _detailTiles.SetTile(tile, _detailDisabledTiles[index]);
                if (_debugDetails)
                    Debug.Log($"Setting right of {tile}");
            }

            if (!locked[1, 0] && !locked[0, 1] && !locked[2, 1] && locked[1, 2])
            {
                // XXX
                // 000
                // 000
                var index = 3;
                if (_detailDisabledTiles.Count > index)
                    _detailTiles.SetTile(tile, _detailDisabledTiles[index]);
                if (_debugDetails)
                    Debug.Log($"Setting top of {tile}");
            }

            // 2 side locked

            if (locked[1, 0] && locked[0, 1] && !locked[2, 1] && !locked[1, 2])
            {
                // X00
                // X00
                // XXX
                var index = 4;
                if (_detailDisabledTiles.Count > index)
                    _detailTiles.SetTile(tile, _detailDisabledTiles[index]);
                if (_debugDetails)
                    Debug.Log($"Setting LB of {tile}");
            }

            if (!locked[1, 0] && locked[0, 1] && !locked[2, 1] && locked[1, 2])
            {
                // XXX
                // X00
                // X00
                var index = 5;
                if (_detailDisabledTiles.Count > index)
                    _detailTiles.SetTile(tile, _detailDisabledTiles[index]);
                if (_debugDetails)
                    Debug.Log($"Setting LT of {tile}");
            }

            if (!locked[1, 0] && !locked[0, 1] && locked[2, 1] && locked[1, 2])
            {
                // XXX
                // 00X
                // 00X
                var index = 6;
                if (_detailDisabledTiles.Count > index)
                    _detailTiles.SetTile(tile, _detailDisabledTiles[index]);
                if (_debugDetails)
                    Debug.Log($"Setting TR of {tile}");
            }

            if (locked[1, 0] && !locked[0, 1] && locked[2, 1] && !locked[1, 2])
            {
                // 00X
                // 00X
                // XXX
                var index = 7;
                if (_detailDisabledTiles.Count > index)
                    _detailTiles.SetTile(tile, _detailDisabledTiles[index]);
                if (_debugDetails)
                    Debug.Log($"Setting RB of {tile}");
            }

            // here the other configurations of 2sided tiles could go but they wont be in our demo

            // 3 side locked

            if (locked[1, 0] && locked[0, 1] && !locked[2, 1] && locked[1, 2])
            {
                // XXX
                // X00
                // XXX
                var index = 8;
                if (_detailDisabledTiles.Count > index)
                    _detailTiles.SetTile(tile, _detailDisabledTiles[index]);
                if (_debugDetails)
                    Debug.Log($"Setting LTB of {tile}");
            }

            if (!locked[1, 0] && locked[0, 1] && locked[2, 1] && locked[1, 2])
            {
                // XXX
                // X0X
                // X0X
                var index = 9;
                if (_detailDisabledTiles.Count > index)
                    _detailTiles.SetTile(tile, _detailDisabledTiles[index]);
                if (_debugDetails)
                    Debug.Log($"Setting LRT of {tile}");
            }

            if (locked[1, 0] && !locked[0, 1] && locked[2, 1] && locked[1, 2])
            {
                // XXX
                // 00X
                // XXX
                var index = 10;
                if (_detailDisabledTiles.Count > index)
                    _detailTiles.SetTile(tile, _detailDisabledTiles[index]);
                if (_debugDetails)
                    Debug.Log($"Setting RTB of {tile}");
            }

            if (locked[1, 0] && locked[0, 1] && locked[2, 1] && !locked[1, 2])
            {
                // X0X
                // X0X
                // XXX
                var index = 11;
                if (_detailDisabledTiles.Count > index)
                    _detailTiles.SetTile(tile, _detailDisabledTiles[index]);
                if (_debugDetails)
                    Debug.Log($"Setting LRB of {tile}");
            }

            // 4 side locked

            if (locked[1, 0] && locked[0, 1] && locked[2, 1] && locked[1, 2])
            {
                // XXX
                // X0X
                // XXX
                var index = 12;
                if (_detailDisabledTiles.Count > index)
                    _detailTiles.SetTile(tile, _detailDisabledTiles[index]);
                if (_debugDetails)
                    Debug.Log($"Setting LRTB of {tile}");
            }
        }

        private bool IsWaterTile(TileBase tile)
        {
            if (tile == null)
                return false;

            foreach (var t in _watertiles)
            {
                if (tile.name == t.name)
                    return true;
            }

            return false;
        }

        private void Awake()
        {
            var renderer = _tiles.GetComponent<TilemapRenderer>();
            renderer.receiveShadows = true;
            renderer.shadowCastingMode = ShadowCastingMode.On;
            GenerateDetails();
        }

        #endregion
    }
}
