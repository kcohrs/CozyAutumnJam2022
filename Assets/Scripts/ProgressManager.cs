using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using AutumnJam.Field;
using AutumnJam.Field.Placeables;
using AutumnJam.Progression;
using AutumnJam.Utility;

using UnityEngine;

using Random = UnityEngine.Random;

namespace AutumnJam
{
    public class ProgressManager : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        private Color _summerColor = new Color(.984f, .98f, .682f);
        [SerializeField]
        private Color _fallColor = new Color(.98f, .78f, .651f);
        [SerializeField]
        private Color _winterColor = new Color(.678f, .925f, .886f);

        [SerializeField]
        private int _year;

        [SerializeField]
        private int _turn;
        [SerializeField]
        private int _yearEndTurn;

        [SerializeField]
        private FieldState _field;

        [SerializeField]
        private List<Placeable> UnlockedPlaceables;
        [SerializeField]
        private List<Placeable> AllPlaceables;
        [SerializeField]
        private List<YearDefinition> Years;

        [SerializeField]
        private Placeable _selectedPlaceable;
        private HudManager _hudManager;
        [SerializeField]
        private YearDefinition _currentYear;

        [SerializeField]
        private int _victoryPoints;

        [SerializeField]
        private float _storedFood;

        private List<MousePickup> _leftoverPickups;

        #endregion

        #region Constructors

        public ProgressManager()
        {
            Instance = this;

            UnlockedPlaceables = new List<Placeable>();
        }

        #endregion

        #region Properties

        public int Year => _year;

        public int VictoryPoints
        {
            get => _victoryPoints;
            private set
            {
                _victoryPoints = value;
                _hudManager?.UpdateVictoryPointCount(VictoryPoints);
            }
        }

        public float StoredFood
        {
            get => _storedFood;
            private set
            {
                _storedFood = value;
                _hudManager?.UpdateFoodCount(StoredFood);
            }
        }

        // lmao singleton funni
        public static ProgressManager Instance { get; private set; }

        public bool IsEndingYear { get; private set; }

        public bool IsEndingLastYear { get; private set; }

        public Vector3 VictoryPointsHudLocation { get; private set; }

        public Vector3 FoodHudLocation { get; private set; }

        public bool IsYearProlonged { get; set; }

        #endregion

        #region Public Methods

        public void ResetProgress()
        {
            _year = 0;
            _turn = 0;
            UnlockedPlaceables.Clear();
            IsEndingYear = false;
            IsEndingLastYear = false;
            _currentYear = null;
            _victoryPoints = 0;
            _storedFood = 0;
            _hudManager.ResetCounters();

            var pickups = FindObjectsOfType<MousePickup>();
            while (pickups.Any())
                Destroy(pickups.First().gameObject);
            var housingPickups = FindObjectsOfType<HousingPickUp>();
            while (housingPickups.Any())
                Destroy(housingPickups.First().gameObject);

            _hudManager.ClearPlaceables();
        }

        public void EndTurn()
        {
            // do stuffs here

            if (!GameStateManager.Instance.IsPlaying || IsEndingYear)
                return;

            _field.HandleEndOfTurn();
            _turn++;

            var percentage = (float)_turn / _yearEndTurn;

            _hudManager.SetYearProgress(percentage);

            SetWorldColor(percentage);

            if (_turn >= _yearEndTurn)
                StartCoroutine(EndYear());
        }

        public void AddTurnsToYear(int additionalTurns)
        {
            _yearEndTurn += additionalTurns;

            var percentage = (float)_turn / _yearEndTurn;
            _hudManager.SetYearProgress(percentage);
            SetWorldColor(percentage);
        }

        public void StartNextYear()
        {
            _year++;

            IsEndingYear = false;
            IsEndingLastYear = false;

            _currentYear = Years.FirstOrDefault(x => x.Year == _year);

            // todo maybe add support for endless play?
            if (_currentYear == null)
            {
                _currentYear = new YearDefinition
                {
                    Year = _year,
                    Turns = 20,
                    Unlocks = new List<Placeable>(),
                    RegionUnlocks = new List<Vector2Int>()
                };
            }

            _yearEndTurn = _currentYear.Turns;
            _turn = 0;

            foreach (var newPlaceable in _currentYear.Unlocks)
                AddUnlock(newPlaceable);

            _field.HandleStartOfYear();
            TileManager.Instance.ToggleWinter(false);
            TileManager.Instance.UnlockRegions(_currentYear.RegionUnlocks);
            SetWorldColor(0f);
            _hudManager.SetYearProgress(0f);

            if (_year > 1)
                _hudManager.HandleStartOfYear();
            _hudManager.SetYearText(_year);

            _hudManager.UpdateFoodCount(StoredFood);
            _hudManager.UpdateVictoryPointCount(VictoryPoints);
            _hudManager.UpdateUnlocks();
        }

        public void UnlockPlaceable(Placeable placeable)
        {
            if (StoredFood >= placeable.UnlockPrice)
            {
                StoredFood -= placeable.UnlockPrice;
                AddUnlock(placeable);
            }
            else
                Debug.LogWarning("Tried to unlock placeable but could not afford it.");
        }

        public bool PurchasePlaceable(Placeable placeable)
        {
            if (StoredFood >= placeable.Price)
            {
                StoredFood -= placeable.Price;
                return true;
            }

            Debug.LogWarning("Tried to buy placeable but could not afford it.");

            return false;
        }

        public void RefundPlaceable(Placeable placeable)
        {
            StoredFood += placeable.Price;
        }

        public void SelectPlaceable(Placeable selected)
        {
            // we match the enum precisely
            var placeable = UnlockedPlaceables.FirstOrDefault(x => x.Name == selected.Name);

            _selectedPlaceable = placeable;

            _hudManager.SetSelectedPlaceable(_selectedPlaceable);
        }

        public void SelectShovel()
        {
            _selectedPlaceable = UnlockedPlaceables.First(x => x.Category == PlaceableCategory.Shovel);
            _hudManager.SetShovelSelected();
        }

        public Placeable GetSelectedPlaceable()
        {
            return _selectedPlaceable;
        }

        public void ResetSelectedPlaceable()
        {
            _selectedPlaceable = null;
            _hudManager.SetSelectedPlaceable(null);
        }

        public void SetupRandomBackground()
        {
            for (var i = 0; i < _field.SizeX; i++)
            {
                for (var j = 0; j < _field.SizeY; j++)
                {
                    // filter to prevent all tiles from being filled
                    if (!(Random.value > 0.5f))
                        continue;

                    var placeable = AllPlaceables[Random.Range(0, AllPlaceables.Count)];

                    if (placeable.LimitedToWater && !TileManager.Instance.IsAdjacentToWater(new Vector3Int(i, j)))
                        continue;

                    _field.ForcePlantAtLocation(i, j, placeable, Random.Range(0, 10));

                    var mouseOverHandler = _field.gameObject.GetComponent<MouseOverHandler>();
                    if (mouseOverHandler != null)
                        mouseOverHandler.enabled = false;
                }
            }

            SetWorldColor(0.5f);
        }

        public void ClearFieldState(bool removeAll)
        {
            // bool removeAll to account for stuff dying at end of years

            for (var i = 0; i < _field.SizeX; i++)
            {
                for (var j = 0; j < _field.SizeY; j++)
                {
                    if (removeAll)
                        _field.ForcePlantAtLocation(i, j, null, 0);
                    // do something with the plant info here
                }
            }
        }

        public void AddPickup(Food food, int victoryPoints)
        {
            if (food != null)
                StoredFood += food.Amount;

            VictoryPoints += victoryPoints;
        }

        public int GetStoredFood()
        {
            return Convert.ToInt32(Math.Floor(StoredFood));
        }

        public bool IsPlaceableUnlocked(Placeable placeable)
        {
            return UnlockedPlaceables.Any(x => x.Name == placeable.Name);
        }

        public void TrackLeftoverFood(MousePickup mousePickup)
        {
            // track all lying about stuffs until they are gone
            _leftoverPickups ??= new List<MousePickup>();

            if (!_leftoverPickups.Contains(mousePickup))
                _leftoverPickups.Add(mousePickup);
        }

        #endregion

        #region Non-Public Methods

        private void AddUnlock(Placeable placeable)
        {
            if (UnlockedPlaceables.All(x => x.Name != placeable.Name))
            {
                UnlockedPlaceables.Add(placeable);

                _hudManager.AddNewUnlockedPlaceable(placeable);
            }
        }

        private void SetWorldColor(float percentage)
        {
            // set all colors between three values: 0 = green (summer), 0.5 = brown (fall), 1 = gray (winter)
            // if time set sprite variants based on 0.33 & 0.66
            Color color;

            if (percentage < .5f)
            {
                // green to brown
                percentage *= 2;
                color = Color.Lerp(_summerColor, _fallColor, percentage);
            }
            else
            {
                // brown to gray
                percentage -= .5f;
                percentage *= 2;
                color = Color.Lerp(_fallColor, _winterColor, percentage);
            }

            TileManager.Instance.SetAmbientColor(color);
        }

        private void Awake()
        {
            _hudManager = GetComponent<HudManager>();
            _leftoverPickups = new List<MousePickup>();
            _hudManager.SetSelectedPlaceable(null);
        }

        private IEnumerator FixBug()
        {
            _leftoverPickups.Clear();
            Debug.Log("Before: " + _leftoverPickups.Count);
            yield return new WaitForSeconds(2f);
            Debug.Log("After: " + _leftoverPickups.Count);
            if (_leftoverPickups.Any<MousePickup>())
            { yield return StartCoroutine(nameof(FixBug)); }
        }
        private IEnumerator EndYear()
        {
            IsEndingYear = true;
            _leftoverPickups.Clear();
            IsYearProlonged = false;
            if (!_currentYear.IsLastYear)
                StartCoroutine(ScreenFadeHandler.Instance.FlashFadeScreen(0.1f, 0.1f, 0.1f));
            yield return new WaitForSeconds(.15f);

            AudioManager.Instance.PlaySound(AudioManager.Sound.EndYear);
            FoodHudLocation = _hudManager.GetFoodWorldPos();
            VictoryPointsHudLocation = _hudManager.GetVictoryPointsWorldPos();

            ResetSelectedPlaceable();

            if (_currentYear.IsLastYear)
            {
                IsEndingLastYear = true;
               
                AudioManager.Instance.PlayBackgroundMusic(AudioManager.BackgroundMusic.Highscore);

                _field.HandleEndOfYear();
                TileManager.Instance.ToggleWinter(true);

                yield return StartCoroutine(nameof(FixBug));

                _hudManager.HideHud();

                StartCoroutine(ScreenFadeHandler.Instance.FlashFadeScreen(0.1f, 0.1f, 0.1f));
                yield return new WaitForSeconds(.15f);

                IsEndingYear = false;

                // at end show total score and allow saving
                _hudManager.HandleEndOfGame();
                GameStateManager.Instance.EndCurrentGame(VictoryPoints);
            }
            else
            {
                _field.HandleEndOfYear();
                TileManager.Instance.ToggleWinter(true);
                   
                yield return StartCoroutine(nameof(FixBug));

                _hudManager.HandleEndOfYear();
            }

            yield return 0;

            _hudManager.UpdateUnlocks();

            yield return null;
        }

        #endregion
    }
}
