using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using AutumnJam.Field;
using AutumnJam.Field.Placeables;
using AutumnJam.Progression;
using AutumnJam.Scoring;

using TMPro;

using UnityEngine;
using UnityEngine.UI;
using LootLocker.Requests;


namespace AutumnJam
{
    public class HudManager : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        private GameObject _menuHud;
        [SerializeField]
        private GameObject _gameHud;
        [SerializeField]
        private GameObject _endOfYearHud;
        [SerializeField]
        private GameObject _endOfGameHud;

        [SerializeField]
        private GameObject _menuHudMain;
        [SerializeField]
        private GameObject _menuHudHighScores;
        [SerializeField]
        private GameObject _menuHudOptions;
        [SerializeField]
        private GameObject _menuHudCredits;
        [SerializeField]
        private GameObject _menuHudQuit;

        [SerializeField]
        private Slider _progress;
        [SerializeField]
        private TextMeshProUGUI _yearText;
        [SerializeField]
        private TextMeshProUGUI _selectedPlaceable;

        [SerializeField]
        private TextMeshProUGUI _foodText;
        [SerializeField]
        private TextMeshProUGUI _victoryPointsText;

        [SerializeField]
        private GameObject _shovelButton;

        private List<PurchaseHandler> _purchaseButtons;
        private List<SelectionHandler> _selectionButtons;

        [SerializeField]
        private TextMeshProUGUI _endOfYearText;
        [SerializeField]
        private TextMeshProUGUI _gainedFoodText;
        [SerializeField]
        private TextMeshProUGUI _gainedVictoryPointsText;

        [SerializeField]
        private VerticalLayoutGroup _endingHighScoreDrawer;
        [SerializeField]
        private VerticalLayoutGroup _menuHighScoreDrawer;
        [SerializeField]
        private GameObject _highScoreEntryPrefab;
        [SerializeField]
        private GameObject _newScoreEntryPrefab;

        [SerializeField]
        private ScrollRect _endingHighScoreRect;

        [SerializeField]
        private GameObject _playOptionsPanel;
        private float _targetPercentage;
        private int _actualVictoryPoints;
        private float _actualStoredFood;
        private int _displayedStoredFood;
        private int _displayedVictoryPoints;

        [SerializeField]
        private GameObject _blocker;

        public Leaderboard leaderboard;

        //ID for lootlocker leaderboard stage - 7464, live - 7474
        private int leaderboardID = 7474;

        //show Highscores in MainMenuHUD
        public TextMeshProUGUI playerNamesMainMenu;
        public TextMeshProUGUI playerScoresMainMenu;

        //show Highscores in EndOfGameHUD
        public TextMeshProUGUI playerNamesEndOfGame;
        public TextMeshProUGUI playerScoresEndOfGame;
        public TextMeshProUGUI currentPlayerScore; //Shows Score, even if not top 10

        //set PlayerName for lootlocker
        public TMP_InputField playerNameInputfield;

        #endregion

        #region Constructors

        public HudManager()
        {
            Instance = this;
            _purchaseButtons = new List<PurchaseHandler>();
            _selectionButtons = new List<SelectionHandler>();
        }

        #endregion

        #region Properties

        public static HudManager Instance { get; private set; }

        public bool IsMouseOverUI { get; set; }

        #endregion

        #region Public Methods

        //function to set PlayerName for lootlocker
        public void SetPlayerName()
        {
            LootLockerSDKManager.SetPlayerName(playerNameInputfield.text, (response) =>
            {
                if (response.success)
                {
                    Debug.Log("Successfully set player name");
                    StartCoroutine(FetchTopHighscoresRoutine());
                }
                else
                {
                    Debug.Log("Could not set player name");
                }
            });
            

        }

        public void StartNewGame()
        {
            StartCoroutine(FadeOutMenu());
        }

        public void QuitGameToMenu()
        {
            CloseOptionsDuringPlay();

            // do other teardown first
            if (!_menuHud.activeInHierarchy)
                StartCoroutine(FadeInMenu());
        }

        public void HandleEndOfYear()
        {
            _gameHud.SetActive(false);
            _endOfYearHud.SetActive(true);
        }

        public void HandleStartOfYear()
        {
            _gameHud.SetActive(true);
            _endOfYearHud.SetActive(false);
            IsMouseOverUI = false;
        }

        public void SetYearProgress(float percentage)
        {
            _targetPercentage = percentage;
            StartCoroutine(IncreaseProgress());
        }

        public void SetYearText(int year)
        {
            _yearText.text = $"Year {year}";
            _endOfYearText.text = $"Year {year} finished!";
        }

        public void AddNewUnlockedPlaceable(Placeable placeable)
        {
            if (placeable.Category == PlaceableCategory.Shovel)
                _shovelButton.SetActive(true);

            UpdateUnlocks();
        }

        public void RemoveUnlockedPlaceable(Placeable placeable)
        {
            if (placeable.Category == PlaceableCategory.Shovel)
                _shovelButton.SetActive(false);

            UpdateUnlocks();
        }

        public void SetSelectedPlaceable(Placeable selectedPlaceable)
        {
            _selectedPlaceable.text = selectedPlaceable != null ? $"Place {selectedPlaceable.Name}" : string.Empty;

            UpdateUnlocks();
        }

        public void SetShovelSelected()
        {
            // modify mouse here as well?
            _selectedPlaceable.text = "Shovel selected";

            UpdateUnlocks();
        }

        public void UpdateFoodCount(float storedFood)
        {
            _actualStoredFood = storedFood;

            UpdateUnlocks();
        }

        public void UpdateVictoryPointCount(int victoryPoints)
        {
            _actualVictoryPoints = victoryPoints;
        }

        public Vector3 GetFoodWorldPos()
        {
            return Camera.main.ScreenToWorldPoint(_foodText.rectTransform.position);
        }

        public Vector3 GetVictoryPointsWorldPos()
        {
            return Camera.main.ScreenToWorldPoint(_victoryPointsText.rectTransform.position);
        }

        public void UpdateUnlocks()
        {
            foreach (var button in _purchaseButtons)
                button?.UpdateState();

            foreach (var button in _selectionButtons)
                button?.UpdateState();
        }

        public void HideHud()
        {
            _menuHud.SetActive(false);
            _gameHud.SetActive(false);
            _endOfYearHud.SetActive(false);
            _endOfGameHud.SetActive(false);
        }

        public void HandleEndOfGame()
        {
            StartCoroutine(leaderboard.SubmitScoreRoutine(_actualVictoryPoints));
            currentPlayerScore.text = _actualVictoryPoints.ToString();
            StartCoroutine(FetchTopHighscoresRoutine());
            _menuHud.SetActive(false);
            _gameHud.SetActive(false);
            _endOfYearHud.SetActive(false);
            _endOfGameHud.SetActive(true);      
        }

        public void ReturnToMainMenu()
        {
            _menuHud.SetActive(true);
            _menuHudMain.SetActive(true);
            _menuHudHighScores.SetActive(false);
            _menuHudOptions.SetActive(false);
            _menuHudCredits.SetActive(false);
            _menuHudQuit.SetActive(false);
        }

        public void ShowQuitPrompt()
        {
            _menuHudQuit.SetActive(true);
        }

        public void ShowOptions()
        {
            _menuHudMain.SetActive(false);
            _menuHudOptions.SetActive(true);
        }

        public void ShowOptionsDuringPlay()
        {
            _blocker.SetActive(true);
            _playOptionsPanel.SetActive(true);
        }

        public void CloseOptionsDuringPlay()
        {
            _blocker.SetActive(false);
            _playOptionsPanel.SetActive(false);
            IsMouseOverUI = false;
        }

        public void ShowCredits()
        {
            _menuHudMain.SetActive(false);
            _menuHudCredits.SetActive(true);
        }

        public void ShowHighScores()
        {
            _menuHudMain.SetActive(false);
            _menuHudHighScores.SetActive(true);           
        }

        public void ClearPlaceables()
        {
            _shovelButton.SetActive(false);

            UpdateUnlocks();
        }

        public void RegisterSelectionHandler(SelectionHandler selectionHandler)
        {
            _selectionButtons.Add(selectionHandler);
        }

        public void RegisterPurchaseHandler(PurchaseHandler purchaseHandler)
        {
            _purchaseButtons.Add(purchaseHandler);
        }

        public void ResetCounters()
        {
            // hack to make it update
            _displayedStoredFood = 1;
            _displayedVictoryPoints = 1;
            _actualStoredFood = 0;
            _actualVictoryPoints = 0;
        }

        //gets the names and highscores from lootlocker
        //also sets the textmeshpro components for MainMenuHUD and GameEndHUD
        public IEnumerator FetchTopHighscoresRoutine()
        {
            bool done = false;
            LootLockerSDKManager.GetScoreList(leaderboardID, 10, 0, (response) =>
            {
                if (response.success)
                {
                    string tempPlayerNames = "Name\n";
                    string tempPlayerScores = "Score\n";

                    //makes an array with all highscores
                    LootLockerLeaderboardMember[] members = response.items;

                    for (int i = 0; i < members.Length; i++)
                    {
                        tempPlayerNames += members[i].rank + ". ";
                        if (members[i].player.name != "")
                        {
                            tempPlayerNames += members[i].player.name;
                        }
                        else
                        {
                            tempPlayerNames += members[i].player.id;
                        }
                        tempPlayerScores += members[i].score + "\n";
                        tempPlayerNames += "\n";
                    }
                    done = true;
                    playerNamesMainMenu.text = tempPlayerNames;
                    playerScoresMainMenu.text = tempPlayerScores;
                    playerNamesEndOfGame.text = tempPlayerNames;
                    playerScoresEndOfGame.text = tempPlayerScores;
                }
                else
                {
                    Debug.Log("Failed" + response.Error);
                    done = true;
                }
            });
            //waits for server communication
            yield return new WaitWhile(() => done == false);
        }

        #endregion

        #region Non-Public Methods

        private void Update()
        {
            var actualFood = Convert.ToInt32(Mathf.Floor(_actualStoredFood));

            if (actualFood != _displayedStoredFood)
            {
                if (actualFood > _displayedStoredFood)
                {
                    _displayedStoredFood++;

                    // move faster if possible
                    if (actualFood > _displayedStoredFood)
                        _displayedStoredFood++;

                    AudioManager.Instance.PlaySound(AudioManager.Sound.FoodGained);
                }
                else
                {
                    _displayedStoredFood--;

                    // move faster if possible
                    if (actualFood < _displayedStoredFood)
                        _displayedStoredFood--;
                }

                _foodText.text = $"{_displayedStoredFood}";
                _gainedFoodText.text = $"{_displayedStoredFood}";
                _foodText.fontSize = Math.Clamp(_foodText.fontSize + 1f, 32, 48);
            }
            else
            {
                if (_foodText.fontSize > 32)
                    _foodText.fontSize = Math.Clamp(_foodText.fontSize - 0.5f, 32, 48);
            }

            if (_actualVictoryPoints != _displayedVictoryPoints)
            {
                if (_actualVictoryPoints > _displayedVictoryPoints)
                {
                    _displayedVictoryPoints++;
                    AudioManager.Instance.PlaySound(AudioManager.Sound.VictoryPointGained);
                }
                else
                    _displayedVictoryPoints--;

                _victoryPointsText.text = $"{_displayedVictoryPoints}";
                _gainedVictoryPointsText.text = $"{_displayedVictoryPoints}";
                _victoryPointsText.fontSize = Math.Clamp(_foodText.fontSize + 1f, 32, 48);
            }
            else
            {
                if (_victoryPointsText.fontSize > 32)
                    _victoryPointsText.fontSize = Math.Clamp(_victoryPointsText.fontSize - 0.5f, 32, 48);
            }
        }

        private IEnumerator IncreaseProgress()
        {
            while (Math.Abs(Math.Round(_targetPercentage - _progress.value, 3)) > 0)
            {
                _progress.value = Mathf.Lerp(_progress.value, _targetPercentage, 0.5f);
                yield return 0;
            }
        }

        private void Start()
        {
            _menuHud.SetActive(false);
            _gameHud.SetActive(false);
            _endOfYearHud.SetActive(false);
            _endOfGameHud.SetActive(false);

            _menuHudMain.SetActive(true);
            _menuHudHighScores.SetActive(false);
            _menuHudOptions.SetActive(false);
            _menuHudQuit.SetActive(false);
            _menuHudCredits.SetActive(false);

            _playOptionsPanel.SetActive(false);
            _blocker.SetActive(false);
        }

        private IEnumerator FadeInMenu()
        {
            yield return 0;

            _menuHud.SetActive(true);
            _gameHud.SetActive(false);
            _endOfYearHud.SetActive(false);
            _endOfGameHud.SetActive(false);
        }

        private IEnumerator FadeOutMenu()
        {
            yield return new WaitForSeconds(.1f);

            _menuHud.SetActive(false);

            yield return new WaitForSeconds(.1f);

            _gameHud.SetActive(true);
            _endOfYearHud.SetActive(false);
            _endOfGameHud.SetActive(false);
        }

        #endregion
    }
}
