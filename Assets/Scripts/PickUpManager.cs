using System.Collections;
using System.Collections.Generic;

using AutumnJam.Field;
using AutumnJam.Utility;

using UnityEngine;

namespace AutumnJam
{
    public class PickUpManager : MonoBehaviour
    {
        #region Enums

        public enum PickupType
        {
            Food,
            Victory,
        }

        #endregion

        #region Fields

        public static PickUpManager Instance;

        [SerializeField]
        private GameObject _foodMousePickUpPrefab;
        [SerializeField]
        private GameObject _victoryPointMousePickUpPrefab;
        [SerializeField]
        private GameObject _foodHousingPickUpPrefab;

        [SerializeField]
        private float _foodPerPickUp = 1;
        [SerializeField]
        private int _victoryPointsPerPickUp = 1;

        #endregion

        #region Constructors

        public PickUpManager()
        {
            Instance = this;
        }

        #endregion

        #region Public Methods

        public void SpawnPlayerFoodPickUp(Vector3 pos, float amount, FoodType foodType)
        {
            StartCoroutine(SpawnFoodPickUps(pos, amount, foodType));
        }

        public void SpawnPlayerVictoryPointPickUp(Vector3 pos, int amount)
        {
            StartCoroutine(SpawnVictoryPointPickUps(pos, amount));
        }

        public IEnumerator SpawnHousePickUp(FoodType type, float amount, Vector3 origin, FieldPlot targetHousing)
        {
            while (amount > 0f)
            {
                var value = amount > _foodPerPickUp ? _foodPerPickUp : amount;
                amount -= _foodPerPickUp;

                var pickUp = Instantiate(_foodHousingPickUpPrefab, origin + new Vector3(0f, 0.25f, -.1f), Quaternion.identity).GetComponent<HousingPickUp>();
                pickUp.SetUpTarget(origin.y, type, value, targetHousing);
                yield return new WaitForSeconds(0.1f);
            }
        }

        public void AnimateHousePickUp(Food food, Vector3 origin, List<FieldPlot> targetHousings)
        {
            var fraction = food.Amount / targetHousings.Count;
            foreach (var plot in targetHousings)
                StartCoroutine(SpawnHousePickUp(food.Type, fraction, origin, plot));
        }

        #endregion

        #region Non-Public Methods

        private IEnumerator SpawnFoodPickUps(Vector3 pos, float amount, FoodType foodType)
        {
            while (amount > 0f)
            {
                var value = amount > _foodPerPickUp ? _foodPerPickUp : amount;
                amount -= _foodPerPickUp;
                AudioManager.Instance.PlaySound(AudioManager.Sound.FoodProduced);
                var pickUp = Instantiate(_foodMousePickUpPrefab, pos + new Vector3(0f, 0.25f, -.1f), Quaternion.identity).GetComponent<MousePickup>();
                pickUp.SetUpFood(pos.y, foodType, value);
                yield return new WaitForSeconds(0.1f);
            }
        }

        private IEnumerator SpawnVictoryPointPickUps(Vector3 pos, int amount)
        {
            while (amount > 0f)
            {
                var value = amount > _victoryPointsPerPickUp ? _victoryPointsPerPickUp : amount;
                amount -= _victoryPointsPerPickUp;

                var pickUp = Instantiate(_victoryPointMousePickUpPrefab, pos + new Vector3(0f, 0.25f, -.1f), Quaternion.identity).GetComponent<MousePickup>();
                pickUp.SetUpVictoryPoints(pos.y, value);
                yield return new WaitForSeconds(0.1f);
            }
        }

        #endregion
    }
}
