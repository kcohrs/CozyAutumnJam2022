using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LootLocker.Requests;

namespace AutumnJam.Scoring
{
    public class Leaderboard : MonoBehaviour
    {
        int leaderboardID = 7474;

        public IEnumerator SubmitScoreRoutine(int score)
        {
            bool done = false;
            string playerID = PlayerPrefs.GetString("PlayerID");
            Debug.Log(playerID);
            LootLockerSDKManager.SubmitScore(playerID, score, leaderboardID, (response) =>
            {
                if (response.success)
                {
                    Debug.Log("Successfully uploaded score");
                    done = true;
                }
                else
                {
                    Debug.Log("Failed" + response.Error);
                    done = true;
                }
            });
            yield return new WaitWhile(() => done == false);
        }
    }
}
